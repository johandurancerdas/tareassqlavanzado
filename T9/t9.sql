drop table ordenes_tabla;
drop type ordenes_tipo force;
drop type detalle_orden_tabla force;
drop type detalle_orden_tipo force;
drop type persona_tipo force;
/
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 1
La empresa quiere mejorar el seguimiento de los clientes y le preocupa el que algunos clientes empleados y proveedores puedan ser la misma persona y que los datos, por lo que nos est� pidiendo 
incluir todos los datos comunes en una sola tabla de objetos personas y 
hacer que las tablas de clientes, empleados y proveedores sean tablas de objetos que hereden de personas.
****************************************************************************************************************************************************************************************************************************************************************************************/me suena a que lo que se quiere es que si una persona pertenece a varias instancias a la vez entonces no se duplique la informaci�n y el (empleado, cliente, proveedor) tengo un puntero al cliente REF.
CREATE OR REPLACE TYPE persona_tipo AS OBJECT (
    id                              NVARCHAR2(50),
    id_tipo_identificacion          NUMBER(38),
    fecha_ingreso                   DATE,
    fecha_nacimiento_constitucion   DATE
);
/
ALTER  TYPE persona_tipo NOT FINAL CASCADE;
/
CREATE OR REPLACE TYPE proveedores_fisicos_tipo UNDER PERSONA_TIPO(
NOMBRE                            NVARCHAR2(150) ,
PRIMER_APELLIDO                   NVARCHAR2(50)  ,
SEGUNDO_APELLIDO                  NVARCHAR2(50)  ,
ES_EMPLEADO_INTERNO               NCHAR(1)       ,
SEXO                              NCHAR(1) 
);
/
CREATE OR REPLACE TYPE clientes_fisicos_tipo UNDER PERSONA_TIPO(
NOMBRE                            NVARCHAR2(150) ,
PRIMER_APELLIDO                   NVARCHAR2(50)  ,
SEGUNDO_APELLIDO                  NVARCHAR2(50)  ,
ES_EMPLEADO_INTERNO               NCHAR(1)       ,
SEXO                              NCHAR(1) 
);
/
CREATE OR REPLACE TYPE proveedores_juridicos_tipo UNDER PERSONA_TIPO(
RAZON_SOCIAL                    NVARCHAR2(150)
);
/
CREATE OR REPLACE TYPE clientes_juridicos_tipo UNDER PERSONA_TIPO(
RAZON_SOCIAL                    NVARCHAR2(150), 
CANTIDAD_EMPLEADOS              NUMBER(38)  
);
/
--Creacion de las tablas
CREATE TABLE proveedores_Fisicos_tabla of proveedores_fisicos_tipo;
CREATE TABLE clientes_Fisicos_tabla of clientes_fisicos_tipo;
CREATE TABLE proveedores_juridicos_tabla of proveedores_juridicos_tipo;
CREATE TABLE clientes_juridicos_tabla of clientes_juridicos_tipo;

--Inserci�n de los datos
insert into proveedores_fisicos_tabla (id, id_tipo_identificacion, fecha_ingreso, fecha_nacimiento_constitucion, nombre, primer_apellido, segundo_apellido, es_empleado_interno, sexo)
(select p.identificacion_proveedor, p.id_tipo_identificacion, p.fecha_ingreso, p.fecha_nacimiento_constitucion, pf.nombre, pf.primer_apellido, pf.segundo_apellido, pf.es_empleado_interno, pf.sexo
from proveedores_fisicos pf join proveedores p on p.id_tipo_identificacion = pf.id_tipo_identificacion and p.identificacion_proveedor = pf.identificacion_proveedor);

insert into clientes_fisicos_tabla (id, id_tipo_identificacion, fecha_ingreso, fecha_nacimiento_constitucion, nombre, primer_apellido, segundo_apellido, es_empleado_interno, sexo)
(select c.id_cliente, c.id_tipo_identificacion, c.fecha_ingreso, c.fecha_nacimiento_constitucion, cf.nombre, cf.primer_apellido, cf.segundo_apellido, cf.es_empleado_interno, cf.sexo
from clientes_fisicos cf join clientes c on c.id_tipo_identificacion = cf.id_tipo_identificacion and c.id_cliente = cf.id_cliente);

select * from proveedores_juridicos_tabla;
select * from clientes;
insert into clientes_juridicos_tabla (id, id_tipo_identificacion, fecha_ingreso, fecha_nacimiento_constitucion, razon_social, cantidad_empleados)
(select c.id_cliente, c.id_tipo_identificacion, c.fecha_ingreso, c.fecha_nacimiento_constitucion, cj.razon_social, cj.cantidad_empleados 
from clientes_juridicos cj join clientes c on c.id_tipo_identificacion = cj.id_tipo_identificacion and c.id_cliente = cj.id_cliente);

insert into proveedores_juridicos_tabla (id, id_tipo_identificacion, fecha_ingreso, fecha_nacimiento_constitucion, razon_social)
(select p.identificacion_proveedor, p.id_tipo_identificacion, p.fecha_ingreso, p.fecha_nacimiento_constitucion, pj.razon_social
from proveedores_juridicos pj join proveedores p on p.id_tipo_identificacion = pj.id_tipo_identificacion and p.identificacion_proveedor = pj.identificacion_proveedor);

--Prueba de las tablas 
select * from proveedores_Fisicos_tabla;
select * from clientes_Fisicos_tabla;
select * from proveedores_juridicos_tabla;
select * from clientes_juridicos_tabla;
/
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 2
Adem�s la empresa quiere tambi�n migrar la facturaci�n a una sola tabla objeto relacional, para facilitar la recuperaci�n de los detalles de las �rdenes. 
Haga una nueva versi�n de las �rdenes y detalles de �rdenes que est�n contenidas en tablas anidadas.  
****************************************************************************************************************************************************************************************************************************************************************************************/
CREATE OR REPLACE TYPE detalle_orden_tipo AS OBJECT (
    id_orden       NUMBER,
    linea_orden    NUMBER,
    cod_producto   NVARCHAR2(12),
    cantidad       NUMBER,
    monto          NUMBER
);
/
CREATE OR REPLACE TYPE detalle_orden_tabla as table of detalle_orden_tipo;
/
CREATE OR REPLACE TYPE ordenes_tipo AS OBJECT (
    id_orden                   NUMBER(38),
    id_oficina                 NUMBER(38),
    id_cliente                 NVARCHAR2(50),
    id_tipo_identificacion     NUMBER(38),
    fecha_hora_orden           DATE,
    fecha_hora_envio           DATE,
    cantidad                   NUMBER(38),
    fecha_hora_entrega         DATE,
    total                      NUMBER,
    descuento                  NUMBER(5, 2),
    impuesto                   NUMBER,
    total_general              NUMBER,
    id_direccion_facturacion   NUMBER(38),
    id_direccion_entrega       NUMBER(38),
    detalles detalle_orden_tabla,
    MAP MEMBER FUNCTION comparar RETURN NUMBER, PRAGMA restrict_references ( comparar, wnds, trust )
);
/
CREATE OR REPLACE TYPE BODY ordenes_tipo AS
    MAP MEMBER FUNCTION comparar RETURN NUMBER IS
    BEGIN
        RETURN total;
    END;

END;
/

CREATE TABLE ordenes_tabla OF ordenes_tipo
NESTED TABLE detalles STORE AS detalles_orden;
/
/*Insertar los datos en las nuevas tablas de objetos*/
DECLARE 
    detalles_tabla detalle_orden_tabla;
begin 
    for orden_i IN(SELECT * FROM ordenes where id_orden in (26947, 26948, 26949, 26950)) LOOP
        detalles_tabla := detalle_orden_tabla();
        insert into ordenes_tabla (id_orden, id_oficina, id_cliente, id_tipo_identificacion,fecha_hora_orden, fecha_hora_envio, cantidad, fecha_hora_entrega, total, descuento, impuesto, total_general, id_direccion_facturacion, id_direccion_entrega)
        values(orden_i.id_orden, orden_i.id_oficina, orden_i.id_cliente, orden_i.id_tipo_identificacion, orden_i.fecha_hora_orden, orden_i.fecha_hora_envio, orden_i.cantidad, orden_i.fecha_hora_entrega, orden_i.total, orden_i.descuento, orden_i.impuesto, orden_i.total_general, orden_i.id_direccion_facturacion, orden_i.id_direccion_entrega);
        for detalle_i in (select * from detalle_orden where id_orden = orden_i.id_orden)LOOP
            detalles_tabla.extend;
            detalles_tabla(detalles_tabla.count) := detalle_orden_tipo(detalle_i.id_orden, detalle_i.linea_orden, detalle_i.cod_producto, detalle_i.cantidad, detalle_i.monto);     
        end loop;
        update ordenes_tabla set detalles = detalles_tabla where id_orden = orden_i.id_orden;
    end loop;
end;
/
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 3
Se necesita que se haga el m�todo ORDER o MAP para comparar entre las �rdenes, de manera que la orden que tenga un total mayor sea la mayor.
Haga los cambios que se requieran para poder lograr este prop�sito.
****************************************************************************************************************************************************************************************************************************************************************************************/
--Incluida en punto anterior
 /
--    INSERT INTO ordenes_tabla VALUES (1,1,'ab',1,SYSDATE,SYSDATE,12,SYSDATE,1,1,1,1,1,1, detalle_orden_tabla(detalle_orden_tipo(4, 4, 'Z', 4, 4), detalle_orden_tipo(4, 4, 'Z', 4, 4), detalle_orden_tipo(4, 4, 'Z', 4, 4))); 