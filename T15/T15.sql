--Crear caso de uso en la BD factoraas, en este caso haciendo uso de la tabla direcciones.
--Se agrega una nueva columa de tipo SDO_GEOMETRY que va a contener el punto, notese que al ser coordenadas terrestres se define el tipo de SRID 4326 el cual se utiliza como demostración ya que no viene definido en el material.
--Por lo tanto los resultados pueden ser incosistentes
ALTER TABLE direcciones ADD (
    ubicacion SDO_GEOMETRY
);

SELECT
    latitud,
    longitud,
    ubicacion
FROM
    direcciones;

UPDATE direcciones d
SET
    ubicacion = sdo_geometry(2001, 4326, mdsys.sdo_point_type(d.latitud, d.longitud, NULL), NULL, NULL);

--Se agrega al columna a la tabla user_sdo_geom_metadata

INSERT INTO user_sdo_geom_metadata (
    table_name,
    column_name,
    diminfo,
    srid
) VALUES (
    'direcciones',
    'ubicacion',
    sdo_dim_array(sdo_dim_element('Long', - 180, 180, 0.5), sdo_dim_element('Lat', - 90, 90, 0.5)),
    4326   -- SRID
);

--Se crea un indice sobre la columna espacial

CREATE INDEX direcciones_ubicacion_idx ON
    direcciones (
        ubicacion
    )
        INDEXTYPE IS mdsys.spatial_index;

--Consulta de prueba

SELECT
    latitud,
    longitud,
    ubicacion
FROM
    direcciones;

--Consulta para obtener todos los puntos dentro de un rango

SELECT
    d1.id_direccion   direccion_base,
    d2.id_direccion   direccion_cercana,
    dc1.id_cliente    cliente_base,
    dc2.id_cliente    cliente_cercano,
    sdo_geom.sdo_distance(d1.ubicacion, d2.ubicacion, 0.005, 'unit=KM') distancia
FROM
    direcciones              d1,
    direcciones              d2,
    direccion_cliente        dc1,
    direccion_cliente        dc2,
    user_sdo_geom_metadata   m1
WHERE
    dc1.id_direccion = d1.id_direccion
    AND dc2.id_direccion = d2.id_direccion
    AND sdo_geom.within_distance(d1.ubicacion, m1.diminfo, 5, d2.ubicacion, m1.diminfo, 'unit=KM') = 'TRUE'
    AND dc1.id_cliente = 105036670
    AND d1.id_direccion <> d2.id_direccion;