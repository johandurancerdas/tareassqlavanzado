--------------------------------------
--Uso de coordenadas geograficas terrestres de longitud y latitud.
--Se debe utilizar el valor 4326 *SRID* en el segundo parametro de SDO_GEOMETRY.
--Todas las funciones vistas siguen funcionando.

--------------------------------------
--Ejemplo de creaci�n de punto
/*
select MDSYS.SDO_GEOMETRY(2001, --2 dimensions, 0 LRS, 01 is a point
                   4326, --SRID for Earth lat/lon system
                   MDSYS.SDO_POINT_TYPE(0, 0,NULL), --point located at Lon-0,Lat-0
                   NULL, --not used for a point
                   NULL) --not used for a point
    from dual; 
*/

--Ejecuci�n
select MDSYS.SDO_GEOMETRY(2001,4326, MDSYS.SDO_POINT_TYPE(0, 0,NULL),NULL, NULL) from dual; --longitud 0 grados y latitud 0 grados.
select MDSYS.SDO_GEOMETRY(2001,4326, MDSYS.SDO_POINT_TYPE(-69.322920,26.846786,NULL),NULL, NULL) from dual; -- longitud -69.322920 y latitud 26.846786 (triangulo de las bermudas)



-------------------------------
--Ejemplo de creaci�n de l�nea
/*
select
MDSYS.SDO_GEOMETRY(2002, --2 dimensiones, 0 sin sistema de referencia lineal, 02 es una linea
                   4326, --SRID para sistema de longitud y latitud de la Tierra
                   NULL, -- Solo se usa para definir un punto
                   MDSYS.SDO_ELEM_INFO_ARRAY(1, --inicie con la primera coordenada que se le brinde
                                             2, --haga una linea
                                             1), --linea simple y recta basada en dos puntos
                   MDSYS.SDO_ORDINATE_ARRAY(0,0, --primera coordenada
                                            0.9,0)) --coordenadas finales
from dual;
*/
--Ejecuci�n
select
MDSYS.SDO_GEOMETRY(2002, 4326,NULL,MDSYS.SDO_ELEM_INFO_ARRAY(1,2,1), MDSYS.SDO_ORDINATE_ARRAY(0,0, 0.9,0)) 
from dual;

-------------------------------
--Ejemplo de creaci�n de rectangulo
/*
select MDSYS.SDO_GEOMETRY(2003,
                          4326,
                          NULL,
                          MDSYS.SDO_ELEM_INFO_ARRAY(1, --incie con la primera coordenada
                                                    1003, --cree un poligono exterior
                                                    3), --de forma rectangular a partir de dos esquinas
                          MDSYS.SDO_ORDINATE_ARRAY(0,0, 0.9,0.9)) -� esquinas del rectangulo
  from dual;
 */  
--Ejecucion
select MDSYS.SDO_GEOMETRY(2003,
                          4326,
                          NULL,
                          MDSYS.SDO_ELEM_INFO_ARRAY(1, 
                                                    1003, 
                                                    3), 
                          MDSYS.SDO_ORDINATE_ARRAY(0,0, 0.9,0.9))
  from dual;

-------------------------------
---Ejemplo de creaci�n de rect�ngulo definiendo sus esquinas con puntos
/*
select MDSYS.SDO_GEOMETRY(2003,
                          4326,
                          NULL,
                          MDSYS.SDO_ELEM_INFO_ARRAY(1, --inicie con la primera coordenada
                                                    1003, --dibuje un poligono exterior
                                                    1), --es un poligono simple y se brindan los vertices (primero y ultimo son el mismo punto)
                          MDSYS.SDO_ORDINATE_ARRAY(0,0, 
                                                   0.9,0,
                                                   0.9,0.9,
                                                   0,0.9,
                                                   0,0))
  from dual;
  */
  --Ejecucion
  select MDSYS.SDO_GEOMETRY(2003,
                          4326,
                          NULL,
                          MDSYS.SDO_ELEM_INFO_ARRAY(1, 
                                                    1003, 
                                                    1), 
                          MDSYS.SDO_ORDINATE_ARRAY(0,0, 
                                                   0.9,0,
                                                   0.9,0.9,
                                                   0,0.9,
                                                   0,0))
  from dual;
  
------------------------------------------------------  
--Ejemplo de pol�gono irregular
/*
select MDSYS.SDO_GEOMETRY(2003,
                          4326,
                          NULL,
                          MDSYS.SDO_ELEM_INFO_ARRAY(1, --inicie con la primera coordenada
                                                    1003, --cree un poligono exterior
                                                    1), --un poligono simple a partir de los vertices
                          MDSYS.SDO_ORDINATE_ARRAY(0,0,  -�punto inicial
                                                   0.2,-0.34,
                                                   0.5,0.14,
                                                   0.93,-0.21,
                                                   0.87,0.39,
                                                   0.96,0.48,
                                                   0.75,0.91,
                                                   0.62,0.81,
                                                   0.48,0.96,
                                                   0.1,0.89,
                                                  -0.2,0.67,
                                                   0.1,0.31,
                                                   0,0)) -�el ultimo punto debe ser igual al primero.
  from dual;
  */
  
  --Ejecucion
  select MDSYS.SDO_GEOMETRY(2003,
                          4326,
                          NULL,
                          MDSYS.SDO_ELEM_INFO_ARRAY(1, 
                                                    1003, 
                                                    1), 
                          MDSYS.SDO_ORDINATE_ARRAY(0,0, 
                                                   0.2,-0.34,
                                                   0.5,0.14,
                                                   0.93,-0.21,
                                                   0.87,0.39,
                                                   0.96,0.48,
                                                   0.75,0.91,
                                                   0.62,0.81,
                                                   0.48,0.96,
                                                   0.1,0.89,
                                                  -0.2,0.67,
                                                   0.1,0.31,
                                                   0,0))
  from dual;
  
 ----------------------------------------------------------------------------------------------------------
 --La distancia entre dos puntos geograficos se calcula como lo vimos en los ejemplos anteriores, entre dos puntos de geometria que tienen el mismo SRID, especificando un nivel de tolerancia y opcionalmente una unidad de medida, si no se especifica una generalmente este valor se expresa en metros.
   SELECT 
   SDO_GEOM.SDO_DISTANCE(
    MDSYS.SDO_GEOMETRY(2001,4326, MDSYS.SDO_POINT_TYPE(0, 0,NULL),NULL, NULL), 
    MDSYS.SDO_GEOMETRY(2001,4326, MDSYS.SDO_POINT_TYPE(-69.322920,26.846786,NULL),NULL, NULL), 
    0.005, 
    'unit=KM')
    FROM DUAL; 