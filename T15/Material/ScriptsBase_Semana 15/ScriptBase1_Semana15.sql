--Ejemplo tomado de https://docs.oracle.com/database/121/SPATL/simple-example-inserting-indexing-and-querying-spatial-data.htm#SPATL486 el 9/12/2018
--Traducci�n y comentarios adicionales por UCenfotec.

/*
Se crea una tabla para almacenar los mercados (areas) donde se planea 
distribuir cada tipo de refresco o donde se har� las campa�as, cada fila 
es un �rea de inter�s para una finalidad, los nombres de las tablas y las 
columnas que tendr�n datos espaciales se recomienda que se mantengan simples, 
sin espacios o caracteres especiales ya que estos no son soportados 
en algunas caracteristicas avanzadas.
*/
CREATE TABLE cola_markets (
  mkt_id NUMBER PRIMARY KEY,
  name VARCHAR2(32),
  shape SDO_GEOMETRY);

/* 
El insert a continuaci�n crea un �rea de interes para el refresco Cola A.
Esta �rea es rectangular, la raz�n de incluir esta �rea la define el problema de negocio
*/

INSERT INTO cola_markets VALUES(
  1,
  'cola_a',
  SDO_GEOMETRY(
    2003,  --El entero que se especifica como primer valor del constructor de SDO_GEOMETRY indica en este caso que la geometr�a que estamos creando es un pol�gono de dos dimensiones (en el material de la semana est�n todos los otros tipos).
    NULL, -- Este valor indica el tipo de sistema de coordenadas que se utilizara, en este caso no estaremos estableciendo uno, por ejemplo 4326 son coordenadas de long y lat terrestre.
    NULL, -- Este valor se usa cuando la geometr�a que estamos definiendo es �nicamente un punto, en caso contrario usamos los siguientes dos par�metros. Si estamos definiendo otras geometr�as diferentes de punto, entonces este valor debe estar en NULL.
    SDO_ELEM_INFO_ARRAY(1,1003,3), -- En el ordinate array que sigue tome a partir del primer elemento (1), y cree un rect�ngulo (1003 = exterior) basado en dos puntos (3) (https://docs.oracle.com/database/121/SPATL/sdo_geometry-object-type.htm#GUID-270AE39D-7B83-46D0-9DD6-E5D99C045021__BGHDGCCE)
    SDO_ORDINATE_ARRAY(1,1, 5,7) -- coordenadas x y y de los dos puntos que se requieren para formar el rect�ngulo. (lower left , upper right)
  )
);

--Los sigueintes insert agregan dos �reas m�s pero cambian el tercer par�metro de SDO_ELEM_INFO_ARRAY a 1 lo que indica que SDO_ORDINATE_ARRAY define uno a uno los v�rtices del pol�gono y termina en el punto inicial nuevamente (5,1)

INSERT INTO cola_markets VALUES(
  2,
  'cola_b',
  SDO_GEOMETRY(
    2003,  -- two-dimensional polygon
    NULL,
    NULL,
    SDO_ELEM_INFO_ARRAY(1,1003,1), -- one polygon (exterior polygon ring)
    SDO_ORDINATE_ARRAY(5,1, 8,1, 8,6, 5,7, 5,1)
  )
);

INSERT INTO cola_markets VALUES(
  3,
  'cola_c',
  SDO_GEOMETRY(
    2003,  -- two-dimensional polygon
    NULL,
    NULL,
    SDO_ELEM_INFO_ARRAY(1,1003,1), -- one polygon (exterior polygon ring)
    SDO_ORDINATE_ARRAY(3,3, 6,3, 6,5, 4,5, 3,3)
  )
);

-- Y finalmente se inserta una �rea para Cola D que es un c�rculo de radio 2 . completamente aparte de las otras �reas.

INSERT INTO cola_markets VALUES(
  4,
  'cola_d',
  SDO_GEOMETRY(
    2003,  -- two-dimensional polygon
    NULL,
    NULL,
    SDO_ELEM_INFO_ARRAY(1,1003,4), -- el valor 4 indica que es un c�rculo definido por 3 puntos que forman parte de su circunferencia.
    SDO_ORDINATE_ARRAY(8,7, 10,9, 8,11)
  )
);

---------------------------------------------------------------------------
-- UPDATE METADATA VIEW --
---------------------------------------------------------------------------
-- Cuando se ha creado una tabla con datos espaciales y queremos luego poder indexarla necesitamos actualizar la vista USER_SDO_GEOM_METADATA 
-- Esto se debe hacer una �nica vez por cada capa (combinaci�n de tabla y columna espacial ).

INSERT INTO user_sdo_geom_metadata
    (TABLE_NAME,
     COLUMN_NAME,
     DIMINFO,
     SRID)
  VALUES (
  'cola_markets',
  'shape',
  SDO_DIM_ARRAY(   -- 20X20 grid
    SDO_DIM_ELEMENT('X', 0, 20, 0.005),
    SDO_DIM_ELEMENT('Y', 0, 20, 0.005)
     ),
  NULL   -- SRID
);

-------------------------------------------------------------------
-- CREATE THE SPATIAL INDEX -- El indice espacial es sencillo solo requiere especificar el nombre de la tabla y columna y especificar un tipo especial.
-------------------------------------------------------------------
CREATE INDEX cola_spatial_idx
   ON cola_markets(shape)
   INDEXTYPE IS MDSYS.SPATIAL_INDEX;
-- Preceding statement created an R-tree index.


select * from cola_markets

