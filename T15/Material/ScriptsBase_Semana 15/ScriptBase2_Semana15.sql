

-------------------------------------------------------------------
-- Ejemplos basicos de consultas espaciales --
--El valor 0.005 en todos los ejemplos siguientes corresponde a un valor de tolerancia que indica que tanto se considerara detalle se considerara para tener dos objetos que se consideren iguales. Este valor 0.005 es tipico para coordenadas geograficas, cambiarlo hara mas o menos sensible a la exactitud a las operaciones de datos espaciales.
-------------------------------------------------------------------
-- Devuelve la interseccion de dos geometrias
SELECT SDO_GEOM.SDO_INTERSECTION(c_a.shape, c_c.shape, 0.005)
   FROM cola_markets c_a, cola_markets c_c 
   WHERE c_a.name = 'cola_a' AND c_c.name = 'cola_c';


-- Retornar las �reas de todas las zonas.
SELECT name, SDO_GEOM.SDO_AREA(shape, 0.005) FROM cola_markets;

-- Retornar el �rea del la zona Cola_A.
SELECT c.name, SDO_GEOM.SDO_AREA(c.shape, 0.005) FROM cola_markets c 
   WHERE c.name = 'cola_a';

-- Cu�l es la distancia entre dos geometr�as?
SELECT SDO_GEOM.SDO_DISTANCE(c_b.shape, c_d.shape, 0.005)
   FROM cola_markets c_b, cola_markets c_d
   WHERE c_b.name = 'cola_b' AND c_d.name = 'cola_d';

-- Es v�lida una geometr�a? (podr�a ser inv�lida si los puntos se salen del mapa o carecen de sentido.
SELECT c.name, SDO_GEOM.VALIDATE_GEOMETRY_WITH_CONTEXT(c.shape, 0.005)
   FROM cola_markets c WHERE c.name = 'cola_c';

-- Es v�lida la capa? (todos los datos de la columna (Se crea una tabla de resultados que ocupa el m�todo primero.)
CREATE TABLE val_results (sdo_rowid ROWID, result VARCHAR2(2000));
CALL SDO_GEOM.VALIDATE_LAYER_WITH_CONTEXT('COLA_MARKETS', 'SHAPE', 
  'VAL_RESULTS', 2);
SELECT * from val_results;
  
  
  ---Calcular la distancia entre dos geometrias.
  --distancia minima
  SELECT SDO_GEOM.SDO_DISTANCE(c_b.shape, c_d.shape, 0.005)
   FROM cola_markets c_b, cola_markets c_d
   WHERE c_b.name = 'cola_b' AND c_d.name = 'cola_d';
   
   
   --distancia maxima
   SELECT SDO_GEOM.SDO_MAXDISTANCE(c_b.shape, c_d.shape, 0.005)
  FROM cola_markets c_b, cola_markets c_d
  WHERE c_b.name = 'cola_b' AND c_d.name = 'cola_d';
  
  --determinar si dos objetos estan a una cierta distancia o menos (pruebe modificando la distancia a 0.8 en vez de 1), se requiere la vista de metadatos user_sdo_geom_metadata para poder ver el contexto de la posicion de los elementos. Debemos haber incluido la tabla y columna que estamos utilizando previamente, una sola vez, como se hizo en el primer script
SELECT SDO_GEOM.WITHIN_DISTANCE(c_b.shape, m.diminfo, 1,
     c_d.shape, m.diminfo) 
  FROM cola_markets c_b, cola_markets c_d, user_sdo_geom_metadata m 
  WHERE m.table_name = 'COLA_MARKETS' AND m.column_name = 'SHAPE' 
  AND c_b.name = 'cola_b' AND c_d.name = 'cola_d';
  
  
  
  --XOR entre geometrias (toda el area que este en una sola de las geometrias pero no en las dos)
  SELECT SDO_GEOM.SDO_XOR(c_a.shape, m.diminfo, c_c.shape, m.diminfo) 
  FROM cola_markets c_a, cola_markets c_c, user_sdo_geom_metadata m 
  WHERE m.table_name = 'COLA_MARKETS' AND m.column_name = 'SHAPE' 
  AND c_a.name = 'cola_a' AND c_c.name = 'cola_c';
  
  --Diferencia de geometrias, elimine todo lo que la geometria 1 (c_a) comparta con la geometria 2 (c_c)
  SELECT SDO_GEOM.SDO_DIFFERENCE(c_a.shape, m.diminfo, c_c.shape, m.diminfo) 
  FROM cola_markets c_a, cola_markets c_c, user_sdo_geom_metadata m 
  WHERE m.table_name = 'COLA_MARKETS' AND m.column_name = 'SHAPE' 
  AND c_a.name = 'cola_a' AND c_c.name = 'cola_c';
  
  --Calcule la interseccion de geometrias (lo que ambas tienen en comun)
  SELECT SDO_GEOM.SDO_INTERSECTION(c_a.shape, c_c.shape, 0.005)
   FROM cola_markets c_a, cola_markets c_c 
   WHERE c_a.name = 'cola_a' AND c_c.name = 'cola_c';
   
--Retorna una geometria que representa la union de dos geometrias juntas
SELECT SDO_GEOM.SDO_UNION(c_a.shape, m.diminfo, c_c.shape, m.diminfo) 
  FROM cola_markets c_a, cola_markets c_c, user_sdo_geom_metadata m 
  WHERE m.table_name = 'COLA_MARKETS' AND m.column_name = 'SHAPE' 
  AND c_a.name = 'cola_a' AND c_c.name = 'cola_c';
  
-- Retorna un punto cualquiera contenido en la superficie de una geometria
SELECT SDO_GEOM.SDO_POINTONSURFACE(c.shape, m.diminfo) 
  FROM cola_markets c, user_sdo_geom_metadata m 
  WHERE m.table_name = 'COLA_MARKETS' AND m.column_name = 'SHAPE' 
  AND c.name = 'cola_a';
  

  
--Retorna el centroide o centro de gravedad de una geometria (por ejemplo el centro del circulo, o de cualquier poligono regular o irregular).

SELECT c.name, SDO_GEOM.SDO_CENTROID(c.shape, m.diminfo) 
  FROM cola_markets c, user_sdo_geom_metadata m 
  WHERE m.table_name = 'COLA_MARKETS' AND m.column_name = 'SHAPE' 
  AND c.name = 'cola_c';
  
--Retorna el minimo circulo que se requiere para contener la geometria (devuelve el circulo mas pequeno que se requeriria para encerrar la geometria que se pasa por parametro)
SELECT c.name, SDO_GEOM.SDO_MBC(c.shape, 0.005) FROM cola_markets c
  WHERE c.name = 'cola_a';
  
  
  --Efectuar un analisis de relacion entre dos geometrias ( en este caso la relacion de la geometria cola_b) con todas las demas y consigo misma en un producto cartesiano
  --un ejemplo
  SELECT c.name,
  SDO_GEOM.RELATE(c.shape, 'determine', c_b.shape, 0.005) relationship 
  FROM cola_markets c, cola_markets c_b WHERE c_b.name = 'cola_b';

  --otro ejemplo
  SELECT c1.name,c2.name,SDO_GEOM.RELATE(c1.shape, 'anyinteract', c2.shape, 0.005) as tienen_relacion
  FROM cola_markets c1, cola_markets c2
  where c1.name <> c2.name;
  
  --otro ejemplo mas
  SELECT c1.name,c2.name,SDO_GEOM.RELATE(c1.shape, 'TOUCH+EQUAL', c2.shape, 0.005) as tienen_relacion
  FROM cola_markets c1, cola_markets c2;
  
/*Se pueden probar varias con un + estos son las pruebas que pueden realizarse:

ANYINTERACT: Returns TRUE if the objects are not disjoint.
CONTAINS: Returns CONTAINS if the second object is entirely within the first object and the object boundaries do not touch; otherwise, returns FALSE.
COVEREDBY: Returns COVEREDBY if the first object is entirely within the second object and the object boundaries touch at one or more points; otherwise, returns FALSE.
COVERS: Returns COVERS if the second object is entirely within the first object and the boundaries touch in one or more places; otherwise, returns FALSE.
DISJOINT: Returns DISJOINT if the objects have no common boundary or interior points; otherwise, returns FALSE.
EQUAL: Returns EQUAL if the objects share every point of their boundaries and interior, including any holes in the objects; otherwise, returns FALSE.
INSIDE: Returns INSIDE if the first object is entirely within the second object and the object boundaries do not touch; otherwise, returns FALSE.
ON: Returns ON if the boundary and interior of a line (the first object) is completely on the boundary of a polygon (the second object); otherwise, returns FALSE.
OVERLAPBDYDISJOINT: Returns OVERLAPBDYDISJOINT if the objects overlap, but their boundaries do not interact; otherwise, returns FALSE.
OVERLAPBDYINTERSECT: Returns OVERLAPBDYINTERSECT if the objects overlap, and their boundaries intersect in one or more places; otherwise, returns FALSE.
TOUCH: Returns TOUCH if the two objects share a common boundary point, but no interior points; otherwise, returns FALSE.
*/
  