/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 1
Cree una consulta recursiva para obtener la lista completa(la oficina y todos sus padres y antecesores)  de todas las oficinas que est�n sobre una oficina de m�nimo nivel de su elecci�n, 
a partir de los cambios que realiz� sobre la tabla de estructura organizacional en las asignaciones anteriores.
****************************************************************************************************************************************************************************************************************************************************************************************/
SELECT
    level,
    eo.*
FROM
    estructura_organizacional eo
--start with id_oficina = 3
WHERE
    level > 1
CONNECT BY
    PRIOR id_oficina = oficina_padre;


/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 2
Cree un bloque an�nimo que use una instrucci�n CTE o cl�usula WITH para crear una consulta que obtenga el id_proveedor de todos los proveedores que podr�amos tener que 
contactar si quisi�ramos negociar nuevos precios de insumos para aumentar nuestra producci�n de un producto cuyo id se establezca en una variable.
****************************************************************************************************************************************************************************************************************************************************************************************/

DECLARE
    v_cod_producto NVARCHAR2(12) := 'ACL546314-MG';
BEGIN
    FOR elem IN (
        WITH products AS (
            SELECT
                p.cod_producto     cod_producto,
                ixprod.id_insumo   id_insumo
            FROM
                productos            p
                JOIN insumos_x_producto   ixprod ON p.cod_producto = ixprod.cod_producto
        )
        SELECT
            prd.cod_producto             cod_producto,
            p.identificacion_proveedor   cod_proveedor
        FROM
            proveedores           p
            JOIN insumos_x_proveedor   ixprov ON p.identificacion_proveedor = ixprov.identificacion_proveedor
                                               AND p.id_tipo_identificacion = ixprov.id_tipo_identificacion
            JOIN insumos               i ON i.id_insumo = ixprov.id_insumo
            JOIN products              prd ON prd.id_insumo = i.id_insumo
        WHERE
            prd.cod_producto = v_cod_producto
    ) LOOP
        dbms_output.put_line('{"codigo del producto": "'
                             || elem.cod_producto
                             || '", "identificacion del proveedor": "'
                             || elem.cod_proveedor
                             || '"}');
    END LOOP;
END;
/
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 3
Cree una consulta usando funciones de ventana, que obtenga cual es el valor absoluto de la diferencia entre el precio de los insumos que se usan para los productos y el promedio de los insumos que se usan para cada producto.
abs(precio de los insumos para productos - avg(insumos que se usan para cada producto))
****************************************************************************************************************************************************************************************************************************************************************************************/

SELECT
    p.cod_producto,
    ixprod.id_insumo,
    ixprov.precio precio_insumo,
    round(abs(ixprov.precio - AVG(ixprov.precio) OVER(
        PARTITION BY p.cod_producto
    )), 4) abs_precio_insumo_precio_insumos_x_producto
FROM
    productos             p
    JOIN insumos_x_producto    ixprod ON p.cod_producto = ixprod.cod_producto
    JOIN insumos               i ON i.id_insumo = ixprod.id_insumo
    JOIN insumos_x_proveedor   ixprov ON i.id_insumo = ixprov.id_insumo;


/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 4
A la consulta de ventana anterior, agr�guele lo necesario para:
    - sacar la misma lista ordenada por producto y por insumo (en ese orden) 
    - y para sacar la diferencia entre cada insumo y el insumo de menor valor inmediatamente anterior en la lista, as� como el insumo de mayor valor inmediatamente posterior en la lista.
****************************************************************************************************************************************************************************************************************************************************************************************/

SELECT
    ROW_NUMBER() OVER(
        PARTITION BY p.cod_producto
        ORDER BY
            ixprod.id_insumo
    ) order_row_in_partition,
    p.cod_producto,
    ixprod.id_insumo,
    ixprov.precio precio_insumo,
    LAG(ixprov.precio, 1, 0) OVER(
        PARTITION BY p.cod_producto
        ORDER BY
            ixprov.precio
    ) prev,
    LEAD(ixprov.precio, 1, 0) OVER(
        PARTITION BY p.cod_producto
        ORDER BY
            ixprov.precio
    ) next,
    ixprov.precio - LAG(ixprov.precio, 1, 0) OVER(
        PARTITION BY p.cod_producto
        ORDER BY
            ixprov.precio
    ) subs_prev,
    ixprov.precio - LEAD(ixprov.precio, 1, 0) OVER(
        PARTITION BY p.cod_producto
        ORDER BY
            ixprov.precio
    ) subs_next,
    round(abs(ixprov.precio - AVG(ixprov.precio) OVER(
        PARTITION BY p.cod_producto
    )), 4) abs_precio_insumo_precio_insumos_x_producto
FROM
    productos             p
    JOIN insumos_x_producto    ixprod ON p.cod_producto = ixprod.cod_producto
    JOIN insumos               i ON i.id_insumo = ixprod.id_insumo
    JOIN insumos_x_proveedor   ixprov ON i.id_insumo = ixprov.id_insumo
WHERE
    p.cod_producto = 'ACL546314-MG'
ORDER BY
    ixprod.cod_producto,
    ixprod.id_insumo;
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 5
Incremente la consulta anterior utilizando ROW_NUMBER(), NTH_VALUE() , RANK() y DENSE_RANK() y explique de qu� se trata cada una con un ejemplo.
****************************************************************************************************************************************************************************************************************************************************************************************/

SELECT
    *
FROM
    (
        SELECT
            p.cod_producto,
            ixprod.id_insumo,
            ixprov.precio precio_insumo,
            LAG(ixprov.precio, 1, 0) OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio
            ) prev,
            LEAD(ixprov.precio, 1, 0) OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio
            ) next,
            ixprov.precio - LAG(ixprov.precio, 1, 0) OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio
            ) subs_prev,
            ixprov.precio - LEAD(ixprov.precio, 1, 0) OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio
            ) subs_next,
            round(abs(ixprov.precio - AVG(ixprov.precio) OVER(
                PARTITION BY p.cod_producto
            )), 4) abs_precio_insumo_precio_insumos_x_producto,
            NTH_VALUE(ixprov.precio, 1) FROM LAST OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
            ) AS precio_mas_alto,
            ROW_NUMBER() OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio DESC
            ) row_by_precio_in_partition,
            RANK() OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio DESC
            ) rank_by_precio,
            DENSE_RANK() OVER(
                PARTITION BY p.cod_producto
                ORDER BY
                    ixprov.precio DESC
            ) dense_rank_by_precio
        FROM
            productos             p
            JOIN insumos_x_producto    ixprod ON p.cod_producto = ixprod.cod_producto
            JOIN insumos               i ON i.id_insumo = ixprod.id_insumo
            JOIN insumos_x_proveedor   ixprov ON i.id_insumo = ixprov.id_insumo
        WHERE
            p.cod_producto = 'ACL546314-MG'
        ORDER BY
            ixprod.cod_producto,
            ixprod.id_insumo
    )
WHERE
    row_by_precio_in_partition = 1;
--    where rank_by_precio in (1,2);
--    where dense_rank_by_precio in (1,2);

--Comentar cada uno de los tres where anteriores para cada uno de los casos de usoL
--   *row_by_precio_in_partition para obtener el precio m�s alto para la particion producto.
--   *rank_by_precio para obtener los dos precios m�s altos para la particion producto.
--   *dense_rank_by_precio para obtener los dos precios m�s altos para la particion producto.
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 6
Cree una funci�n que reciba el nombre de una tabla de la base de datos y que devuelva la cantidad de filas que tiene esta tabla.
****************************************************************************************************************************************************************************************************************************************************************************************/

CREATE OR REPLACE FUNCTION obtener_numero_filas_tabla (
    tabla VARCHAR2
) RETURN NUMBER IS
    consulta_str   VARCHAR2(1000);
    numero_filas   NUMBER;
BEGIN
    consulta_str := 'SELECT COUNT(1) FROM ' || tabla;
    EXECUTE IMMEDIATE consulta_str
    INTO numero_filas;
    RETURN numero_filas;
END;
/

BEGIN
    dbms_output.put_line(obtener_numero_filas_tabla('productos'));
END;
/