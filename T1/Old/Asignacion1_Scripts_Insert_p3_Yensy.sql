/*INSERTAR DATOS PARA PUNTO 3*/
--Telefono principal
INSERT INTO FACTORAAS_DEV.Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TP', TELEFONO_PRINCIPAL, null
FROM    FACTORAAS_DEV.Contactos_Cliente A
WHERE   TELEFONO_PRINCIPAL IS NOT NULL;

--Telefono movil
INSERT INTO FACTORAAS_DEV.Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TM', TELEFONO_MOVIL, null
FROM    FACTORAAS_DEV.Contactos_Cliente A
WHERE   TELEFONO_MOVIL IS NOT NULL;

--Telefono habitacion
INSERT INTO FACTORAAS_DEV.Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TH', TELEFONO_HABITACION, null
FROM    FACTORAAS_DEV.Contactos_Cliente A
WHERE   TELEFONO_HABITACION IS NOT NULL;
    
--Telefono trabajo
INSERT INTO FACTORAAS_DEV.Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TT', TELEFONO_TRABAJO, EXTENSION_TRABAJO
FROM    FACTORAAS_DEV.Contactos_Cliente A
WHERE   TELEFONO_TRABAJO IS NOT NULL;    
    
--CORREO PRINCIPAL
INSERT INTO FACTORAAS_DEV.Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'CP', Correo_principal, null
FROM    FACTORAAS_DEV.Contactos_Cliente A
WHERE   A.Correo_principal IS NOT NULL;    

--CORREO PERSONAL
INSERT INTO FACTORAAS_DEV.Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'CI', Correo_Personal, null
FROM    FACTORAAS_DEV.Contactos_Cliente A
WHERE   A.Correo_Personal IS NOT NULL;    

--CORREO PERSONAL
INSERT INTO FACTORAAS_DEV.Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'CT', Correo_Trabajo, null
FROM    FACTORAAS_DEV.Contactos_Cliente A
WHERE   A.Correo_Trabajo IS NOT NULL; 

COMMIT;
/