--************************************************************************************************************************************************************************************************************************************************************
--Caso 1 Atributo multivalorado. Manejo nulos.
--************************************************************************************************************************************************************************************************************************************************************
ALTER TABLE contactos_cliente SET UNUSED ( telefono_principal,
                                           telefono_movil,
                                           telefono_habitacion,
                                           telefono_trabajo,
                                           extension_trabajo,
                                           correo_principal,
                                           correo_personal,
                                           correo_trabajo );

ALTER TABLE contactos_cliente ADD tipo_contacto NVARCHAR2(100);

ALTER TABLE contactos_cliente ADD valor_contacto NVARCHAR2(150);

ALTER TABLE contactos_cliente ADD comentarios NVARCHAR2(150);

--************************************************************************************************************************************************************************************************************************************************************
--Caso 2
-- Manejo incorrecto de nulos: Es un tema subjetivo porque se necesita saber lo que se estaba pensando hacer
-- Sin embargo dado que las operaciones algebraicas con null resultan en null el tener el descuento en null y aplicar total+impuestos-descuento puede resultar en nulo.
-- Por lo anterior y suponiendo que los nulos presentes en descuento debería ser un valor conocido se aplicará a la base de datos un update para hacerlo cero en caso de ser nulo
--************************************************************************************************************************************************************************************************************************************************************

UPDATE ordenes
SET
    descuento = 0
WHERE
    descuento IS NULL;
    
    
--************************************************************************************************************************************************************************************************************************************************************
--Caso 3
--Generalización migrada a especialización. EAV. Manejo de nulos
--************************************************************************************************************************************************************************************************************************************************************

CREATE TABLE clientes_fisicos (
    id_cliente               NVARCHAR2(50) NOT NULL,
    id_tipo_identificacion   NUMBER(38) NOT NULL,
    nombre                   NVARCHAR2(150),
    primer_apellido          NVARCHAR2(50),
    segundo_apellido         NVARCHAR2(50),
    es_empleado_interno      NCHAR(1),
    sexo                     NCHAR(1),
    CONSTRAINT cliente_fisico_pk PRIMARY KEY ( id_cliente,
                                               id_tipo_identificacion ),
    CONSTRAINT cliente_fisico_fk FOREIGN KEY ( id_cliente,
                                               id_tipo_identificacion )
        REFERENCES clientes ( id_cliente,
                              id_tipo_identificacion )
);

CREATE TABLE clientes_juridicos (
    id_cliente               NVARCHAR2(50) NOT NULL,
    id_tipo_identificacion   NUMBER(38) NOT NULL,
    razon_social             NVARCHAR2(150),
    cantidad_empleados       NUMBER(38),
    CONSTRAINT cliente__pk PRIMARY KEY ( id_cliente,
                                         id_tipo_identificacion ),
    CONSTRAINT cliente_juridico_fk FOREIGN KEY ( id_cliente,
                                                 id_tipo_identificacion )
        REFERENCES clientes ( id_cliente,
                              id_tipo_identificacion )
);

ALTER TABLE clientes DROP ( nombre,
                            primer_apellido,
                            segundo_apellido,
                            sexo,
                            razon_social,
                            segmento_cliente,
                            es_empleado_interno,
                            cantidad_empleados,
                            tipo_cliente_fisico_juridico );

--Editar la tabla clientes_contactos para que se relacione con las subclases y no con la super clase.

ALTER TABLE clientes_contactos DROP CONSTRAINT clientes_contac_clientes_fk;

ALTER TABLE clientes_contactos DROP CONSTRAINT clientes_contac_clientes_fk2;

ALTER TABLE clientes_contactos
    ADD CONSTRAINT clientes_contac_clientes_juridicos_fk FOREIGN KEY ( id_cliente_juridico,
                                                                       tipo_id_cliente_juridico )
        REFERENCES clientes_juridicos ( id_cliente,
                                        id_tipo_identificacion )
    ENABLE NOVALIDATE;

ALTER TABLE clientes_contactos
    ADD CONSTRAINT clientes_contac_clientes_fisicos_fk FOREIGN KEY ( id_cliente_fisico_contacto,
                                                                     tipo_id_cliente_fis_contacto )
        REFERENCES clientes_fisicos ( id_cliente,
                                      id_tipo_identificacion )
    ENABLE NOVALIDATE;

--Editar la tabla clientes_apoderados para que se relacione con las subclases y no con la superclase.

ALTER TABLE clientes_apoderados DROP CONSTRAINT clientes_apod_clientes_fk;

ALTER TABLE clientes_apoderados DROP CONSTRAINT clientes_apod_clientes_fk2;

ALTER TABLE clientes_apoderados
    ADD CONSTRAINT clientes_apod_clientes_fk FOREIGN KEY ( id_cliente_juridico,
                                                           tipo_id_cliente_juridico )
        REFERENCES clientes_juridicos ( id_cliente,
                                        id_tipo_identificacion )
    ENABLE NOVALIDATE;

ALTER TABLE clientes_apoderados
    ADD CONSTRAINT clientes_apod_clientes_fk2 FOREIGN KEY ( id_cliente_fisico_apoderado,
                                                            tipo_id_cliente_fis_apoderado )
        REFERENCES clientes_fisicos ( id_cliente,
                                      id_tipo_identificacion )
    ENABLE NOVALIDATE;
    
    
--************************************************************************************************************************************************************************************************************************************************************
--Caso 4
--Uso incorrecto de llaves
--************************************************************************************************************************************************************************************************************************************************************
ALTER TABLE insumos_x_proveedor DROP CONSTRAINT ins_x_provee_proveedor_fk;

ALTER TABLE insumos_x_proveedor DROP CONSTRAINT insumos_x_proveedor_pk;

ALTER TABLE proveedores DROP CONSTRAINT proveedores_pk;

ALTER TABLE proveedores DROP CONSTRAINT proveedores__un;

ALTER TABLE insumos_x_proveedor DROP COLUMN id_proveedor;

ALTER TABLE proveedores DROP COLUMN id_proveedor;

ALTER TABLE proveedores ADD CONSTRAINT proveedores_pk PRIMARY KEY ( identificacion_proveedor,
                                                                    id_tipo_identificacion );

ALTER TABLE insumos_x_proveedor ADD identificacion_proveedor NVARCHAR2(50);

ALTER TABLE insumos_x_proveedor ADD id_tipo_identificacion NUMBER(38);

ALTER TABLE insumos_x_proveedor
    ADD CONSTRAINT insumos_x_proveedor_pk PRIMARY KEY ( id_insumo,
                                                        identificacion_proveedor,
                                                        id_tipo_identificacion,
                                                        fecha_actualizacion );

ALTER TABLE insumos_x_proveedor
    ADD CONSTRAINT ins_x_provee_proveedor_fk FOREIGN KEY ( identificacion_proveedor,
                                                           id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion );

ALTER TABLE proveedores DROP ( nombre,
                               primer_apellido,
                               segundo_apellido,
                               sexo,
                               razon_social,
                               es_empleado_interno,
                               tipo_proveedor_fisico_juridico );

CREATE TABLE proveedores_fisicos (
    identificacion_proveedor   NVARCHAR2(50) NOT NULL,
    id_tipo_identificacion     NUMBER(38) NOT NULL,
    nombre                     NVARCHAR2(150),
    primer_apellido            NVARCHAR2(50),
    segundo_apellido           NVARCHAR2(50),
    es_empleado_interno        NCHAR(1),
    sexo                       NCHAR(1),
    CONSTRAINT proveedores_fisicos_pk PRIMARY KEY ( identificacion_proveedor,
                                                    id_tipo_identificacion ),
    CONSTRAINT proveedores_fisicos_fk FOREIGN KEY ( identificacion_proveedor,
                                                    id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion )
);

CREATE TABLE proveedores_juridicos (
    identificacion_proveedor   NVARCHAR2(50) NOT NULL,
    id_tipo_identificacion     NUMBER(38) NOT NULL,
    razon_social               NVARCHAR2(150),
    CONSTRAINT proveedores_juridicos_pk PRIMARY KEY ( identificacion_proveedor,
                                                      id_tipo_identificacion ),
    CONSTRAINT proveedores_juridicos_fk FOREIGN KEY ( identificacion_proveedor,
                                                      id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion )
);


--Uso teoricamente incorrecto pero aplicado por temas de rendimiento y organización.