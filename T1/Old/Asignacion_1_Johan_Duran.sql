--************************************************************************************************************************************************************************************************************************************************************
--Caso 2
-- Manejo incorrecto de nulos: Es un tema subjetivo porque se necesita saber lo que se estaba pensando hacer
-- Sin embargo dado que las operaciones algebraicas con null resultan en null el tener el descuento en null y aplicar total+impuestos-descuento puede resultar en nulo.
-- Por lo anterior y suponiendo que los nulos presentes en descuento debería ser un valor conocido se aplicará a la base de datos un update para hacerlo cero en caso de ser nulo
--************************************************************************************************************************************************************************************************************************************************************

UPDATE ordenes
SET
    descuento = 0
WHERE
    descuento IS NULL;
    
    
--************************************************************************************************************************************************************************************************************************************************************
--Caso 3
--Generalización migrada a especialización. EAV. Manejo de nulos
--************************************************************************************************************************************************************************************************************************************************************

CREATE TABLE clientes_fisicos
    AS
        SELECT
            id_cliente,
            id_tipo_identificacion,
            nombre,
            primer_apellido,
            segundo_apellido,
            es_empleado_interno,
            sexo
        FROM
            clientes
        WHERE
            tipo_cliente_fisico_juridico = 'F';

ALTER TABLE clientes_fisicos ADD CONSTRAINT cliente_fisico_pk PRIMARY KEY ( id_cliente,
                                                                            id_tipo_identificacion );

ALTER TABLE clientes_fisicos
    ADD CONSTRAINT cliente_fisico_fk FOREIGN KEY ( id_cliente,
                                                   id_tipo_identificacion )
        REFERENCES clientes ( id_cliente,
                              id_tipo_identificacion );

CREATE TABLE clientes_juridicos
    AS
        SELECT
            id_cliente,
            id_tipo_identificacion,
            razon_social,
            cantidad_empleados
        FROM
            clientes
        WHERE
            tipo_cliente_fisico_juridico = 'J';

ALTER TABLE clientes_juridicos ADD CONSTRAINT cliente__pk PRIMARY KEY ( id_cliente,
                                                                        id_tipo_identificacion );

ALTER TABLE clientes_juridicos
    ADD CONSTRAINT cliente_juridico_fk FOREIGN KEY ( id_cliente,
                                                     id_tipo_identificacion )
        REFERENCES clientes ( id_cliente,
                              id_tipo_identificacion );
                              
--Eliminar las columnas que ahora perteneces a las subclases

ALTER TABLE clientes DROP ( nombre,
                            primer_apellido,
                            segundo_apellido,
                            sexo,
                            razon_social,
                            es_empleado_interno,
                            cantidad_empleados );
    
--************************************************************************************************************************************************************************************************************************************************************
--Caso 4
--Uso incorrecto de llaves
--************************************************************************************************************************************************************************************************************************************************************

ALTER TABLE insumos_x_proveedor DROP CONSTRAINT ins_x_provee_proveedor_fk;

ALTER TABLE insumos_x_proveedor DROP CONSTRAINT insumos_x_proveedor_pk;

ALTER TABLE proveedores DROP CONSTRAINT proveedores_pk;

ALTER TABLE proveedores DROP CONSTRAINT proveedores__un;

ALTER TABLE proveedores ADD CONSTRAINT proveedores_pk PRIMARY KEY ( identificacion_proveedor,
                                                                    id_tipo_identificacion );
ALTER TABLE insumos_x_proveedor ADD identificacion_proveedor NVARCHAR2(50);

ALTER TABLE insumos_x_proveedor ADD id_tipo_identificacion NUMBER(38);

--insertar en insumos_x_proveedor la nueva llave

UPDATE insumos_x_proveedor ixp
SET
    ( ixp.identificacion_proveedor,
      ixp.id_tipo_identificacion ) = (
        SELECT
            p.identificacion_proveedor,
            p.id_tipo_identificacion
        FROM
            proveedores p
        WHERE
            p.id_proveedor = ixp.id_proveedor
    );

ALTER TABLE insumos_x_proveedor
    ADD CONSTRAINT insumos_x_proveedor_pk PRIMARY KEY ( id_insumo,
                                                        identificacion_proveedor,
                                                        id_tipo_identificacion,
                                                        fecha_actualizacion );

ALTER TABLE insumos_x_proveedor
    ADD CONSTRAINT ins_x_provee_proveedor_fk FOREIGN KEY ( identificacion_proveedor,
                                                           id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion );

ALTER TABLE insumos_x_proveedor DROP COLUMN id_proveedor;

ALTER TABLE proveedores DROP COLUMN id_proveedor;
--Convertir proveedor en especializacion

CREATE TABLE proveedores_fisicos
    AS
        SELECT
            identificacion_proveedor,
            id_tipo_identificacion,
            nombre,
            primer_apellido,
            segundo_apellido,
            es_empleado_interno,
            sexo
        FROM
            proveedores
        WHERE
            tipo_proveedor_fisico_juridico = 'F';

ALTER TABLE proveedores_fisicos ADD CONSTRAINT proveedores_fisicos_pk PRIMARY KEY ( identificacion_proveedor,
                                                                                    id_tipo_identificacion );

ALTER TABLE proveedores_fisicos
    ADD CONSTRAINT proveedores_fisicos_fk FOREIGN KEY ( identificacion_proveedor,
                                                        id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion );

CREATE TABLE proveedores_juridicos
    AS
        SELECT
            identificacion_proveedor,
            id_tipo_identificacion,
            razon_social
        FROM
            proveedores
        WHERE
            tipo_proveedor_fisico_juridico = 'J';

ALTER TABLE proveedores_juridicos ADD CONSTRAINT proveedores_juridicos_pk PRIMARY KEY ( identificacion_proveedor,
                                                                                        id_tipo_identificacion );

ALTER TABLE proveedores_juridicos
    ADD CONSTRAINT proveedores_juridicos_fk FOREIGN KEY ( identificacion_proveedor,
                                                          id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion );

ALTER TABLE proveedores DROP ( nombre,
                               primer_apellido,
                               segundo_apellido,
                               sexo,
                               razon_social,
                               es_empleado_interno );
                               