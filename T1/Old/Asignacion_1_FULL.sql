/*******************************************************************************
 1. Manejo incorrecto de nulos 
 El manejo de nulos depende mucho del usuario y la intención de los datos 
 sin embargo dado que las operaciones algebraicas con null resultan en null el tener el descuento en null y aplicar total+impuestos-descuento puede resultar en nulo.
 Por lo anterior y suponiendo que los nulos presentes en descuento deberen ser un valor conocido se aplica a la base de datos un update para hacerlo cero en caso de ser nulo
*******************************************************************************/


UPDATE ordenes
SET
    descuento = 0
WHERE
    descuento IS NULL;
    
    
/*******************************************************************************
2. Clientes: Manejo de nulos y Generalización convertida en especializacion ISA DT
La entidad cliente al estar generalizada tanto para cliente fisico como juridico, perjudica en el hecho de
que los campos son mutuamente excluyentes lo cual  implica tener muchos valores nulos para la otra entidad en cuestión.
Por dicha razón se modifica la estructura de la siguiente forma:
    Super-Clase: Clientes
    Sub-Clase 1: Clientes_Juridicos
    Sub-Clase 2: Clientes_Fisicos
En la Super-Clase se conservan los atributos comunes a ambos: FECHA_NACIMIENTO_CONSTITUCION, SEGMENTO_CLIENTE, FECHA_INGRESO
En la Sub-Clase de Clientes_Fisicos se definen los campos: NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, ES_EMPLEADO_INTERNO, SEXO
En la Sub-Clase de Clientes_Juridicos se definen los campos: RAZON_SOCIAL, CANTIDAD_EMPLEADOS
*******************************************************************************/

--Creacion de las subclases
CREATE TABLE clientes_fisicos
    AS
        SELECT
            id_cliente,
            id_tipo_identificacion,
            nombre,
            primer_apellido,
            segundo_apellido,
            es_empleado_interno,
            sexo
        FROM
            clientes
        WHERE
            tipo_cliente_fisico_juridico = 'F';

CREATE TABLE clientes_juridicos
    AS
        SELECT
            id_cliente,
            id_tipo_identificacion,
            razon_social,
            cantidad_empleados
        FROM
            clientes
        WHERE
            tipo_cliente_fisico_juridico = 'J';

--Inserción de las restricciones para las nuevas tablas
ALTER TABLE clientes_fisicos ADD CONSTRAINT cliente_fisico_pk PRIMARY KEY ( id_cliente,
                                                                            id_tipo_identificacion );

ALTER TABLE clientes_fisicos
    ADD CONSTRAINT cliente_fisico_fk FOREIGN KEY ( id_cliente,
                                                   id_tipo_identificacion )
        REFERENCES clientes ( id_cliente,
                              id_tipo_identificacion );

ALTER TABLE clientes_juridicos ADD CONSTRAINT cliente__pk PRIMARY KEY ( id_cliente,
                                                                        id_tipo_identificacion );

ALTER TABLE clientes_juridicos
    ADD CONSTRAINT cliente_juridico_fk FOREIGN KEY ( id_cliente,
                                                     id_tipo_identificacion )
        REFERENCES clientes ( id_cliente,
                              id_tipo_identificacion );
                              
--Eliminar las columnas que ahora perteneces a las subclases

ALTER TABLE clientes DROP ( nombre,
                            primer_apellido,
                            segundo_apellido,
                            sexo,
                            razon_social,
                            es_empleado_interno,
                            cantidad_empleados );
    
/*******************************************************************************
3. Uso incorrecto de llaves
Al utilizar llaves primarias es muy importante en la medida de lo posible utilizar
valores naturales a la entidad, para el caso de proveedores el mismo contiene una 
llave unica que no es usada. Por dicha razón se procede a modificarla y eliminar 
la llave autogenerada para ser modificada por la identificacion del proveedor.
Proceso:
    Alterar las restricciones
    Crear los nuevos campos del ID del proveedor en la tabla insumos
    Utilizando el antiguo ID actualizar y completar los valores del nuevo.
    Eliminar los campos no requeridos
*******************************************************************************/


ALTER TABLE insumos_x_proveedor DROP CONSTRAINT ins_x_provee_proveedor_fk;

ALTER TABLE insumos_x_proveedor DROP CONSTRAINT insumos_x_proveedor_pk;

ALTER TABLE proveedores DROP CONSTRAINT proveedores_pk;

ALTER TABLE proveedores DROP CONSTRAINT proveedores__un;

ALTER TABLE proveedores ADD CONSTRAINT proveedores_pk PRIMARY KEY ( identificacion_proveedor,
                                                                    id_tipo_identificacion );
ALTER TABLE insumos_x_proveedor ADD identificacion_proveedor NVARCHAR2(50);

ALTER TABLE insumos_x_proveedor ADD id_tipo_identificacion NUMBER(38);

--insertar en insumos_x_proveedor la nueva llave

UPDATE insumos_x_proveedor ixp
SET
    ( ixp.identificacion_proveedor,
      ixp.id_tipo_identificacion ) = (
        SELECT
            p.identificacion_proveedor,
            p.id_tipo_identificacion
        FROM
            proveedores p
        WHERE
            p.id_proveedor = ixp.id_proveedor
    );

ALTER TABLE insumos_x_proveedor
    ADD CONSTRAINT insumos_x_proveedor_pk PRIMARY KEY ( id_insumo,
                                                        identificacion_proveedor,
                                                        id_tipo_identificacion,
                                                        fecha_actualizacion );

ALTER TABLE insumos_x_proveedor
    ADD CONSTRAINT ins_x_provee_proveedor_fk FOREIGN KEY ( identificacion_proveedor,
                                                           id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion );

ALTER TABLE insumos_x_proveedor DROP COLUMN id_proveedor;

ALTER TABLE proveedores DROP COLUMN id_proveedor;

/*******************************************************************************
4. Proveedores: Control de nulos y migracion a especialización de tipo de proveedor.
La entidad proveedores contiene datos de dos sub-entidades en ella misma,
datos de proveedores fisicos y juridicos. En dicha Entidad la mayor parte de los datos
son proveedores fisicos dejando así la mayor parte de los campos vacios. Por dicha razón 
se crearon dos subentidades para cada tipo de proveedor.
Proceso:
    Crear dos tablas nuevas para cada tipo de proveedor con los campos correspondientes
    Altarar las restricciones para generar las relaciones entre ellos.
*******************************************************************************/

CREATE TABLE proveedores_fisicos
    AS
        SELECT
            identificacion_proveedor,
            id_tipo_identificacion,
            nombre,
            primer_apellido,
            segundo_apellido,
            es_empleado_interno,
            sexo
        FROM
            proveedores
        WHERE
            tipo_proveedor_fisico_juridico = 'F';


CREATE TABLE proveedores_juridicos
    AS
        SELECT
            identificacion_proveedor,
            id_tipo_identificacion,
            razon_social
        FROM
            proveedores
        WHERE
            tipo_proveedor_fisico_juridico = 'J';

ALTER TABLE proveedores_fisicos ADD CONSTRAINT proveedores_fisicos_pk PRIMARY KEY ( identificacion_proveedor,
                                                                                    id_tipo_identificacion );

ALTER TABLE proveedores_fisicos
    ADD CONSTRAINT proveedores_fisicos_fk FOREIGN KEY ( identificacion_proveedor,
                                                        id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion );

ALTER TABLE proveedores_juridicos ADD CONSTRAINT proveedores_juridicos_pk PRIMARY KEY ( identificacion_proveedor,
                                                                                        id_tipo_identificacion );

ALTER TABLE proveedores_juridicos
    ADD CONSTRAINT proveedores_juridicos_fk FOREIGN KEY ( identificacion_proveedor,
                                                          id_tipo_identificacion )
        REFERENCES proveedores ( identificacion_proveedor,
                                 id_tipo_identificacion );

ALTER TABLE proveedores DROP ( nombre,
                               primer_apellido,
                               segundo_apellido,
                               sexo,
                               razon_social,
                               es_empleado_interno );

/*******************************************************************************
5.	Crear la tabla Tipos_Clientes
  a. Que tenga las columnas:
    i. Codigo_Tipo_Cliente: identificador no null. Es el c�digo del tipo de cliente.
    ii. Descripcion_Tipo_Cliente: Descripci�n del tipo de cliente.
  b.	Ejemplo:
    Codigo_Tipo_Cliente	|   Descripcion_Tipo_Cliente
            F	        |   Cliente f�sico
            J           |   Cliente Jur�dico
  c. Crear una llave for�nea de la tabla CLIENTES campo Tipo_Cliente_Fisico_Juridico al campo Tipos_Clientes.Codigo_Tipo_Cliente
  d. Descripci�n del cambio: Esto funciona para restringir los datos que pueden ser ingresados en este campo, adem�s de facilitar el mantenimiento y soporte en caso de que a futuro se creen m�s posibles valores.
  e. Soluciona el antipatr�n 31 Sabores
*******************************************************************************/
CREATE TABLE Tipos_Clientes
(
    Codigo_Tipo_Cliente NCHAR(1) NOT NULL,
    Descripcion_Tipo_Cliente NVARCHAR2(50), 
    CONSTRAINT Tipos_Clientes_PK PRIMARY KEY (Codigo_Tipo_Cliente)
) TABLESPACE USERS;

COMMENT ON COLUMN Tipos_Clientes.Codigo_Tipo_Cliente IS 'C�digo del tipo de cliente, por el momento solo existen: F: Fisico y J: Juridico.';

COMMENT ON COLUMN Tipos_Clientes.Descripcion_Tipo_Cliente IS 'Descripcion del tipo de cliente.';

INSERT INTO Tipos_Clientes (Codigo_Tipo_Cliente, Descripcion_Tipo_Cliente)
VALUES ('F', 'FISICO');
INSERT INTO Tipos_Clientes (Codigo_Tipo_Cliente, Descripcion_Tipo_Cliente)
VALUES ('J', 'JURIDICO');

COMMIT;

ALTER TABLE Clientes
ADD CONSTRAINT Clientes_Tipos_Clientes_PK
   FOREIGN KEY (Tipo_Cliente_Fisico_Juridico)
   REFERENCES Tipos_Clientes(Codigo_Tipo_Cliente);
/*******************************************************************************
6.	Crear la tabla Tipos_Contacto 
  a. Columnas:
    i. Codigo_Tipo_Contacto: identificador no null. C�digo del tipo de contacto.
    ii. Descripcion_Tipo_Contacto: Descripcion del tipo de contacto,
  b. Ejemplo:
    Codigo_Tipo_Contacto|   Descripcion_Tipo_Contacto
        TP	            |   Tel�fono Principal
        TM	            |   Tel�fono M�vil
        TH	            |   Tel�fono Habitaci�n
        CP	            |   Correo Principal
        CP	            |   Correo Personal
  c. Descripci�n del motivo del cambio: Esto funciona para restringir los datos que pueden ser ingresados en este campo, adem�s de facilitar el mantenimiento y soporte en caso de que a futuro se creen m�s posibles valores. * Es parte complementaria del cambio 2 y 3.
  d. Soluciona el antipatr�n: 31 Sabores.
*******************************************************************************/
CREATE TABLE Tipos_Contacto
(
    Codigo_Tipo_Contacto NVARCHAR2(5) NOT NULL,
    Descripcion_Tipo_Contacto NVARCHAR2(100), 
    CONSTRAINT Tipos_Contacto_PK PRIMARY KEY (Codigo_Tipo_Contacto)
) TABLESPACE USERS;

COMMENT ON COLUMN Tipos_Contacto.Codigo_Tipo_Contacto IS 'C�digo representativo del tipo de contacto. Ej.: TP: Tel�fono Principal, TM: Tel�fono M�vil, TH: Tel�fono Habitaci�n, TT: Tel�fono Trabajo, CP: Correo Principal, CP: Correo Personal, CT: Correo Trabajo';

COMMENT ON COLUMN Tipos_Contacto.Descripcion_Tipo_Contacto IS 'Descripcion del tipo de contacto.';

INSERT INTO Tipos_Contacto (Codigo_Tipo_Contacto, Descripcion_Tipo_Contacto)
VALUES ('TP','Tel�fono Principal');
INSERT INTO Tipos_Contacto (Codigo_Tipo_Contacto, Descripcion_Tipo_Contacto)
VALUES ('TM','Tel�fono M�vil');
INSERT INTO Tipos_Contacto (Codigo_Tipo_Contacto, Descripcion_Tipo_Contacto)
VALUES ('TH','Tel�fono Habitaci�n');
INSERT INTO Tipos_Contacto (Codigo_Tipo_Contacto, Descripcion_Tipo_Contacto)
VALUES ('TT','Tel�fono Trabajo');
INSERT INTO Tipos_Contacto (Codigo_Tipo_Contacto, Descripcion_Tipo_Contacto)
VALUES ('CP','Correo Principal');
INSERT INTO Tipos_Contacto (Codigo_Tipo_Contacto, Descripcion_Tipo_Contacto)
VALUES ('CI','Correo Personal');
INSERT INTO Tipos_Contacto (Codigo_Tipo_Contacto, Descripcion_Tipo_Contacto)
VALUES ('CT','Correo Trabajo');

COMMIT;
/*******************************************************************************
7.	Crear la tabla Contactos
  a.	Con las columnas:
    i.	ID_Contacto: Identificador �nico no nulo, autonum�rco. Llave principal de esta tabla.
    ii.	ID_Tipo_Contacto: Identificador del tipo de contacto. Tiene una referencia a la tabla Tipos_Contacto.Id_Tipo_Contacto.
    iii. Valor_Contacto: Valor del contacto
    iv.	Num_Extensi�n: n�mero de extensi�n aplica �nicamente para los tel�fonos de trabajo. En caso contrario estar�a vac�o.
    v.	Ejemplo:
    ID_Contacto	| ID_Tipo_Contacto  | Valor_Contacto    | Extensi�n
        1	    |   TT	            | 22225555          |   2613
        2	    |   TM	            | 88445566	        |   
        3	    |   CP	            | yensy@gmail.com   |   
        4	    |   CT	            | yensy@excelsoftsources.com
  a. Descripci�n del motivo del cambio: Es la continuaci�n al cambio 2. La cantidad de formas de contactar a un cliente puede aumentar con el tiempo as� como la forma de contacto.
  b. Soluciona el antipatr�n: Jaywalking
*******************************************************************************/
CREATE TABLE Contactos
(
    ID_Contacto NUMBER GENERATED ALWAYS AS IDENTITY,
    ID_Cliente  NVARCHAR2(50) NOT NULL,
    ID_Tipo_Identificacion NUMBER(*,0) NOT NULL,
    ID_Tipo_Contacto NVARCHAR2(5) NOT NULL,
    Valor_Contacto   NVARCHAR2(150) NOT NULL,
    Num_Extension    NUMBER(*,0) NULL,
    CONSTRAINT Contactos_PK PRIMARY KEY (ID_Contacto),
    CONSTRAINT Contactos_Clientes_FK
        FOREIGN KEY (ID_Cliente,ID_Tipo_Identificacion)
        REFERENCES Clientes(ID_Cliente,ID_Tipo_Identificacion),
    CONSTRAINT Contacto_Tipos_Contacto_FK
        FOREIGN KEY (ID_Tipo_Contacto)
        REFERENCES Tipos_Contacto (Codigo_Tipo_Contacto)
) TABLESPACE USERS;

COMMENT ON COLUMN Contactos.ID_Contacto IS 'Identificador unico del contacto.';
COMMENT ON COLUMN Contactos.ID_Cliente IS 'Numero de identificacion del cliente.';
COMMENT ON COLUMN Contactos.ID_Tipo_Identificacion IS 'Tipo de identificacion del cliente';
COMMENT ON COLUMN Contactos.ID_Tipo_Contacto IS 'Codigo del tipo de contacto.';
COMMENT ON COLUMN Contactos.Valor_Contacto IS 'Valor del tipo de contacto. Ejemplo: numero telefonico, correo, entre otros';
COMMENT ON COLUMN Contactos.Num_Extension IS 'Numero de extension del cliente. actualmente aplica solo para telefonos de trabajo.';

/*INSERTAR DATOS PARA PUNTO 3*/
--Telefono principal
INSERT INTO Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TP', TELEFONO_PRINCIPAL, null
FROM    Contactos_Cliente A
WHERE   TELEFONO_PRINCIPAL IS NOT NULL;

--Telefono movil
INSERT INTO Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TM', TELEFONO_MOVIL, null
FROM    Contactos_Cliente A
WHERE   TELEFONO_MOVIL IS NOT NULL;

--Telefono habitacion
INSERT INTO Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TH', TELEFONO_HABITACION, null
FROM    Contactos_Cliente A
WHERE   TELEFONO_HABITACION IS NOT NULL;
    
--Telefono trabajo
INSERT INTO Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'TT', TELEFONO_TRABAJO, EXTENSION_TRABAJO
FROM    Contactos_Cliente A
WHERE   TELEFONO_TRABAJO IS NOT NULL;    
    
--CORREO PRINCIPAL
INSERT INTO Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'CP', Correo_principal, null
FROM    Contactos_Cliente A
WHERE   A.Correo_principal IS NOT NULL;    

--CORREO PERSONAL
INSERT INTO Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'CI', Correo_Personal, null
FROM    Contactos_Cliente A
WHERE   A.Correo_Personal IS NOT NULL;    

--CORREO PERSONAL
INSERT INTO Contactos (ID_Cliente, ID_Tipo_Identificacion, ID_Tipo_Contacto, Valor_Contacto, Num_Extension)
SELECT  ID_CLIENTE, ID_TIPO_IDENTIFICACION, 'CT', Correo_Trabajo, null
FROM    Contactos_Cliente A
WHERE   A.Correo_Trabajo IS NOT NULL; 

COMMIT;
DROP TABLE contactos_cliente;
/*******************************************************************************
8. Cat�logo de Pa�s, Provincia, Cant�n y Distrito
  a.	Crear la tabla Paises para el manejo del cat�logo de pa�ses
    i.	Columnas:
      1.	Cod_Pais: C�digo del Pa�s
      2.	Nombre_Pais: Nombre del Pa�s
    ii.	Ejemplo:
        Cod_Pais	| Nombre_Pais
        CR	        | Costa Rica
        EEUU	    | Estados Unidos*/
CREATE TABLE Paises
(
    Cod_Pais    NVARCHAR2(10) NOT NULL,
    Nombre_Pais NVARCHAR2(150), 
    CONSTRAINT Paises_PK PRIMARY KEY (Cod_Pais)
) TABLESPACE USERS;

COMMENT ON COLUMN Paises.Cod_Pais IS 'C�digo del pais, por el momento solo existe: CR para COSTA RICA.';

COMMENT ON COLUMN Paises.Nombre_Pais IS 'Nombre del pais.';

INSERT INTO Paises (Cod_Pais, Nombre_Pais)
VALUES ('CR', 'COSTA RICA');

INSERT INTO Paises (Cod_Pais, Nombre_Pais)
VALUES ('EEUU', 'ESTADOS UNIDOS');

COMMIT;
/
/*
  b.	Crear la tabla Provincias para el manejo del cat�logo de provincias
    i.	Columnas:
      1.	Cod_Pais: C�digo del Pa�s
      2.	Cod_Provincia: C�digo del Provincia
      3.	Nombre_Provincia: Nombre del Provincia
    ii.	Ejemplo:
    Cod_Pais	|   Cod_Provincia   | Nombre_Provincia
    CR          |       SJ	        | San Jos�
    CR 	        |       CR	        | Cartago
*/
CREATE TABLE Provincias
(
    Cod_Pais    NVARCHAR2(10) NOT NULL,
    Cod_Provincia    NVARCHAR2(10) NOT NULL,
    Nombre_Provincia NVARCHAR2(150) NOT NULL,
    CONSTRAINT Provincias_PK PRIMARY KEY (Cod_Pais, Cod_Provincia),
    CONSTRAINT Paises_Provincias_FK
        FOREIGN KEY (Cod_Pais)
        REFERENCES Paises(Cod_Pais)
) TABLESPACE USERS;

COMMENT ON COLUMN Provincias.Cod_Pais IS 'C�digo del pais, por el momento solo existe: CR para COSTA RICA.';
COMMENT ON COLUMN Provincias.Cod_Provincia IS 'C�digo de la provincia.';
COMMENT ON COLUMN Provincias.Nombre_Provincia IS 'Nombre de la provincia.';

INSERT INTO Provincias (Cod_Pais, Cod_Provincia, Nombre_Provincia)
VALUES ('CR', 'L', 'LIMON');
INSERT INTO Provincias (Cod_Pais, Cod_Provincia, Nombre_Provincia)
VALUES ('CR', 'S', 'SAN JOS�');
INSERT INTO Provincias (Cod_Pais, Cod_Provincia, Nombre_Provincia)
VALUES ('CR', 'G', 'GUANACASTE');
INSERT INTO Provincias (Cod_Pais, Cod_Provincia, Nombre_Provincia)
VALUES ('CR', 'C', 'CARTAGO');
INSERT INTO Provincias (Cod_Pais, Cod_Provincia, Nombre_Provincia)
VALUES ('CR', 'A', 'ALAJUELA');
INSERT INTO Provincias (Cod_Pais, Cod_Provincia, Nombre_Provincia)
VALUES ('CR', 'H', 'HEREDIA');
INSERT INTO Provincias (Cod_Pais, Cod_Provincia, Nombre_Provincia)
VALUES ('CR', 'P', 'PUNTARENAS');

COMMIT;
/
/*
  c.	Crear la tabla Cant�n para el manejo del cat�logo de cantones
    i.	Columnas:
      1.	Cod_Pais: C�digo del Pa�s
      2.	Cod_Provincia: C�digo del Provincia
      3.	Cod_Canton: C�digo del Cant�n
      4.	Nombre_Canton: Nombre del Cant�n
    ii.	Ejemplo:
        Cod_Pais	Cod_Provincia	Cod_Canton	Nombre_ Canton
        CR	SJ	SP	San Pedro
        CR	CR	LU	La Uni�n
  e.	Descripci�n del motivo del cambio: Esto funciona para restringir los datos que pueden ser ingresados en el campo de pa�ses, adem�s de facilitar el mantenimiento y soporte en caso de que a futuro se creen m�s posibles valores. 
  f.	Soluciona el antipatr�n: 31 Sabores.
*******************************************************************************/
CREATE TABLE Cantones
(
    Cod_Pais        NVARCHAR2(10) NOT NULL,
    Cod_Provincia   NVARCHAR2(10) NOT NULL,
    Cod_Canton      NVARCHAR2(10) NOT NULL,
    Nombre_Canton   NVARCHAR2(150) NOT NULL,
    CONSTRAINT Cantones_PK PRIMARY KEY (Cod_Pais, Cod_Provincia, Cod_Canton),
    CONSTRAINT Canton_Provincia_FK
        FOREIGN KEY (Cod_Pais,Cod_Provincia)
        REFERENCES Provincias(Cod_Pais,Cod_Provincia)
) TABLESPACE USERS;

COMMENT ON COLUMN Cantones.Cod_Pais IS 'C�digo del pais, por el momento solo existe: CR para COSTA RICA.';
COMMENT ON COLUMN Cantones.Cod_Provincia IS 'C�digo de la provincia';
COMMENT ON COLUMN Cantones.Cod_Canton IS 'C�digo del canton.';
COMMENT ON COLUMN Cantones.Nombre_Canton IS 'Nombre del Canton.';

INSERT INTO Cantones (Cod_Pais, Cod_Provincia, Cod_Canton, Nombre_Canton)
SELECT  DISTINCT cod_pais, substr(provincia_estado, 0,1) AS Cod_Provincia, SUBSTR(canton_ciudad,0,10) AS Cod_Canton, canton_ciudad
FROM    Direccion_Cliente A
ORDER BY cod_pais, substr(provincia_estado, 0,1), SUBSTR(canton_ciudad,0,10), canton_ciudad;

COMMIT;
/
/*******************************************************************************
9.	Crear la tabla Direccion
  a.	Tabla para almacenar las direcciones de todos los clientes.
  b.	Columnas:
    i.	ID_Direccion: Identificador autonum�rico no nulo de la direcci�n.
    ii.	Codigo_Postal: C�digo postal de la ubicaci�n
    iii.Cod_Pais: C�digo del pa�s. Llave for�nea a la tabla de paises.
    iv.	Cod_Provincia: C�digo de la provincia. Llave for�nea a la tabla de provincias.
    v.	Cod_Canton: C�digo del cant�n. Llave for�nea a la tabla de cantones.
    vi.	Latitud: Coordenadas de latitud
    vii.Longitud: Coordenadas de Longitud
    viii.Detalles: Direcci�n exacta del lugar,
    ix.	Fecha de actualizaci�n: fecha en que se realiz� la �ltima actualizaci�n
    x.	Fecha de eliminaci�n: Fecha en que se elimin� el registro
  c.	Ejemplo:
ID
Direccion	Codigo_Postal	Cod_Pais	Cod_Provincia	Cod_Canton	Latitud	Longitud	Detalles	Fecha Actualizaci�n	Fecha Eliminaci�n
1	123456	CR	H	ASD	-83.092	8.81421	De la entrada tal 250m.	01/01/2018	NULL
2	654654	CR	A	LKJ	-25.36	89.10	Frente al antiguo X.	15/02/2019	20/02/2019
  d.	Descripci�n del motivo del cambio: La cantidad de v�as para contactar a un cliente puede variar con el tiempo as� como la forma de contacto. * Es parte complementaria del cambio 6.
  e.	Soluciona el antipatr�n: Jaywalking 
*******************************************************************************/
CREATE TABLE Direcciones
(
    ID_Direccion    NUMBER(*,0) NOT NULL, /*NUMBER GENERATED ALWAYS AS IDENTITY,*/
    Codigo_Postal   NUMBER(*,0),
    Cod_Pais        NVARCHAR2(10) NOT NULL,
    Cod_Provincia   NVARCHAR2(10) NOT NULL,
    Cod_Canton      NVARCHAR2(10) NOT NULL,
    Latitud         NUMBER(9,6), 
	Longitud        NUMBER(9,6), 
	Detalles        NVARCHAR2(250), 
    Fecha_Actualizacion DATE NOT NULL ENABLE, 
	Fecha_Eliminacion   DATE,
    CONSTRAINT Direcciones_PK PRIMARY KEY (ID_Direccion),
    /*Se eliminan porque crea redundancia
    CONSTRAINT Direccion_Pais_FK
        FOREIGN KEY (Cod_Pais)
        REFERENCES Paises(Cod_Pais),
    CONSTRAINT Direccion_Provincia_FK
        FOREIGN KEY (Cod_Pais,Cod_Provincia)
        REFERENCES Provincias(Cod_Pais,Cod_Provincia),
    */
    CONSTRAINT Direccion_Canton_FK
        FOREIGN KEY (Cod_Pais,Cod_Provincia, Cod_Canton)
        REFERENCES Cantones(Cod_Pais,Cod_Provincia, Cod_Canton)
) TABLESPACE USERS;

COMMENT ON COLUMN Direcciones.ID_Direccion IS 'Identificador unico de la direccion.';
COMMENT ON COLUMN Direcciones.Codigo_Postal IS 'C�digo postal de la direccion.';
COMMENT ON COLUMN Direcciones.Cod_Pais IS 'C�digo del pais';
COMMENT ON COLUMN Direcciones.Cod_Provincia IS 'C�digo de la provincia';
COMMENT ON COLUMN Direcciones.Cod_Canton IS 'C�digo del canton.';
COMMENT ON COLUMN Direcciones.Latitud IS 'Coordenadas. Latitud de la direccion';
COMMENT ON COLUMN Direcciones.Longitud IS 'Coordenadas. Longitud de la direccion';
COMMENT ON COLUMN Direcciones.Detalles IS 'Direccion exacta.';
COMMENT ON COLUMN Direcciones.Fecha_Actualizacion IS 'Fecha en que se realizo la ultima actualizacion';
COMMENT ON COLUMN Direcciones.Fecha_Eliminacion IS 'Fecha en que se elimino el registro.';

--INGRESAR LAS DIRECCIONES DE LA TABLA DIRECCION_CLIENTE EN LA NUEVA TABLA.
INSERT INTO Direcciones
        (ID_Direccion, Codigo_Postal, Cod_Pais, Cod_Provincia, Cod_Canton,
        Latitud, Longitud, Detalles, Fecha_Actualizacion, Fecha_Eliminacion)
SELECT  ID_Direccion, Codigo_Postal, Cod_Pais, substr(provincia_estado, 0,1), SUBSTR(canton_ciudad,0,10),
        Latitud, Longitud, Detalles, Fecha_Actualizacion, Fecha_Eliminacion
FROM    Direccion_cliente;

COMMIT;
/
/*******************************************************************************
10.	Modificar la tabla Direccion_Cliente
  a. Eliminar las columnas: (codigo_postal, cod_pais, latitud, longitud, detalles, provincia_estado, canton_ciudad, nombre_pais)
  b. Crear llave forenea entre esta tabla y la de direcciones
  c. Modificar para que la llave primaria sea id_direcicion, id_cliente, id_tipo_identificacion
  c. Descripci�n del motivo del cambio: Esto funciona para restringir los datos que pueden ser ingresados en este campo, adem�s de facilitar el mantenimiento y soporte en caso de que a futuro se creen m�s posibles valores. * Es parte complementaria del cambio 5 y 7.
  d. Soluciona el antipatr�n: 31 Sabores.
*******************************************************************************/
ALTER TABLE Direccion_Cliente DROP (codigo_postal, cod_pais, latitud, longitud, detalles, provincia_estado, canton_ciudad, nombre_pais);
ALTER TABLE Direccion_Cliente DROP CONSTRAINT DIRECCION_CLIENTE__UN;
ALTER TABLE ordenes DROP CONSTRAINT ORDENES_DIRECCION_CLIENTE_FK;
ALTER TABLE ordenes DROP CONSTRAINT ORDENES_DIRECCION_CLIENTE_FK2;
ALTER TABLE Direccion_Cliente DROP CONSTRAINT Direccion_Cliente_PK;
ALTER TABLE Direccion_Cliente ADD CONSTRAINT Direccion_Cliente_PK PRIMARY KEY (Id_Direccion, id_Cliente, Id_Tipo_Identificacion);
ALTER TABLE Direccion_Cliente ADD CONSTRAINT DIRECCION_CLIENTE__UN UNIQUE (Id_Direccion, id_Cliente, Id_Tipo_Identificacion, Fecha_Actualizacion);
ALTER TABLE ordenes ADD CONSTRAINT ORDENES_DIRECCION_CLIENTE_FK
   FOREIGN KEY (Id_Direccion_Facturacion, id_Cliente, Id_Tipo_Identificacion)
   REFERENCES Direccion_Cliente(Id_Direccion, id_Cliente, Id_Tipo_Identificacion);
ALTER TABLE ordenes ADD CONSTRAINT ORDENES_DIRECCION_CLIENTE_FK2
   FOREIGN KEY (Id_Direccion_Entrega, id_Cliente, Id_Tipo_Identificacion)
   REFERENCES Direccion_Cliente(Id_Direccion, id_Cliente, Id_Tipo_Identificacion);

ALTER TABLE Direccion_Cliente ADD CONSTRAINT Direcciones_Direccion_Cliente_FK
   FOREIGN KEY (Id_Direccion)
   REFERENCES Direcciones(Id_Direccion);

COMMIT;
/