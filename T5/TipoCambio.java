import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TipoCambio {
    public static String getTipoCambioDolarDelDia() {
        String response = "";
        try {
            String base_url = "http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?" +
                    "tcIndicador=317&" +
                    "tcFechaInicio=" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + "&" +
                    "tcFechaFinal=" +
                    new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + "&" +
                    "tcNombre=Cenfotec&" +
                    "tnSubNiveles=N";

            StringBuilder result = new StringBuilder();
            URL url = new URL(base_url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            response = result.toString();
            response = response.replace("&lt;", "<");
            response = response.replace("&gt;", ">");

            response = response.split("<NUM_VALOR>")[1];
            response = response.split("</NUM_VALOR>")[0];
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
