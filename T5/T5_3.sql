--Para cargar la clase 
--loadjava -user factoraas_test/oracle@PDB1 .\TipoCambio.class
CREATE OR REPLACE FUNCTION tipo_cambio RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'TipoCambio.getTipoCambioDolarDelDia() return java.lang.String';
/
DECLARE
 v_tipo_cambio VARCHAR2(100);
BEGIN
    v_tipo_cambio := tipo_cambio();
    dbms_output.put_line(v_tipo_cambio);
END;

