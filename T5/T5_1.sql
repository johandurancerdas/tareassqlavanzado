--Faltan los Store Procedure para las modificaciones
CREATE OR REPLACE PACKAGE paq_administracion_ordenes IS
    PROCEDURE pr_agregar_orden (
        prm_id_orden                   ordenes.id_orden%TYPE,
        prm_id_oficina                 ordenes.id_oficina%TYPE,
        prm_id_cliente                 ordenes.id_cliente%TYPE,
        prm_id_tipo_identificacion     ordenes.id_tipo_identificacion%TYPE,
        prm_fecha_hora_orden           ordenes.fecha_hora_orden%TYPE,
        prm_fecha_hora_envio           ordenes.fecha_hora_envio%TYPE,
        prm_cantidad                   ordenes.cantidad%TYPE,
        prm_fecha_hora_entrega         ordenes.fecha_hora_entrega%TYPE,
        prm_total                      ordenes.total%TYPE,
        prm_descuento                  ordenes.descuento%TYPE,
        prm_impuesto                   ordenes.impuesto%TYPE,
        prm_total_general              ordenes.total_general%TYPE,
        prm_id_direccion_facturacion   ordenes.id_direccion_facturacion%TYPE,
        prm_id_direccion_entrega       ordenes.id_direccion_entrega%TYPE
    );

    PROCEDURE pr_modificar_orden (
        prm_id_orden                   ordenes.id_orden%TYPE,
        prm_id_oficina                 ordenes.id_oficina%TYPE,
        prm_id_cliente                 ordenes.id_cliente%TYPE,
        prm_id_tipo_identificacion     ordenes.id_tipo_identificacion%TYPE,
        prm_fecha_hora_orden           ordenes.fecha_hora_orden%TYPE,
        prm_fecha_hora_envio           ordenes.fecha_hora_envio%TYPE,
        prm_cantidad                   ordenes.cantidad%TYPE,
        prm_fecha_hora_entrega         ordenes.fecha_hora_entrega%TYPE,
        prm_total                      ordenes.total%TYPE,
        prm_descuento                  ordenes.descuento%TYPE,
        prm_impuesto                   ordenes.impuesto%TYPE,
        prm_total_general              ordenes.total_general%TYPE,
        prm_id_direccion_facturacion   ordenes.id_direccion_facturacion%TYPE,
        prm_id_direccion_entrega       ordenes.id_direccion_entrega%TYPE
    );

    PROCEDURE pr_agregar_detalle_orden (
        prm_id_orden       detalle_orden.id_orden%TYPE,
        prm_linea_orden    detalle_orden.linea_orden%TYPE,
        prm_cod_producto   detalle_orden.cod_producto%TYPE,
        prm_cantidad       detalle_orden.cantidad%TYPE,
        prm_monto          detalle_orden.monto%TYPE
    );

    PROCEDURE pr_modificar_detalle_orden (
        prm_id_orden       detalle_orden.id_orden%TYPE,
        prm_linea_orden    detalle_orden.linea_orden%TYPE,
        prm_cod_producto   detalle_orden.cod_producto%TYPE,
        prm_cantidad       detalle_orden.cantidad%TYPE,
        prm_monto          detalle_orden.monto%TYPE
    );

    PROCEDURE pr_eliminar_orden (
        prm_numero_orden ordenes.id_orden%TYPE
    );

    PROCEDURE pr_eliminar_detalle_orden (
        prm_id_orden   detalle_orden.id_orden%TYPE,
        prm_linea      detalle_orden.linea_orden%TYPE
    );

END paq_administracion_ordenes;
/

CREATE OR REPLACE PACKAGE BODY paq_administracion_ordenes AS 
--1. Procedure para crear ordenes
    PROCEDURE pr_agregar_orden (
        prm_id_orden                   ordenes.id_orden%TYPE,
        prm_id_oficina                 ordenes.id_oficina%TYPE,
        prm_id_cliente                 ordenes.id_cliente%TYPE,
        prm_id_tipo_identificacion     ordenes.id_tipo_identificacion%TYPE,
        prm_fecha_hora_orden           ordenes.fecha_hora_orden%TYPE,
        prm_fecha_hora_envio           ordenes.fecha_hora_envio%TYPE,
        prm_cantidad                   ordenes.cantidad%TYPE,
        prm_fecha_hora_entrega         ordenes.fecha_hora_entrega%TYPE,
        prm_total                      ordenes.total%TYPE,
        prm_descuento                  ordenes.descuento%TYPE,
        prm_impuesto                   ordenes.impuesto%TYPE,
        prm_total_general              ordenes.total_general%TYPE,
        prm_id_direccion_facturacion   ordenes.id_direccion_facturacion%TYPE,
        prm_id_direccion_entrega       ordenes.id_direccion_entrega%TYPE
    ) IS
    BEGIN
        INSERT INTO ordenes (
            id_orden,
            id_oficina,
            id_cliente,
            id_tipo_identificacion,
            fecha_hora_orden,
            fecha_hora_envio,
            cantidad,
            fecha_hora_entrega,
            total,
            descuento,
            impuesto,
            total_general,
            id_direccion_facturacion,
            id_direccion_entrega
        ) VALUES (
            prm_id_orden,
            prm_id_oficina,
            prm_id_cliente,
            prm_id_tipo_identificacion,
            prm_fecha_hora_orden,
            prm_fecha_hora_envio,
            prm_cantidad,
            prm_fecha_hora_entrega,
            prm_total,
            prm_descuento,
            prm_impuesto,
            prm_total_general,
            prm_id_direccion_facturacion,
            prm_id_direccion_entrega
        );

    END;

--2. Procedure para modificar una orden

    PROCEDURE pr_modificar_orden (
        prm_id_orden                   ordenes.id_orden%TYPE,
        prm_id_oficina                 ordenes.id_oficina%TYPE,
        prm_id_cliente                 ordenes.id_cliente%TYPE,
        prm_id_tipo_identificacion     ordenes.id_tipo_identificacion%TYPE,
        prm_fecha_hora_orden           ordenes.fecha_hora_orden%TYPE,
        prm_fecha_hora_envio           ordenes.fecha_hora_envio%TYPE,
        prm_cantidad                   ordenes.cantidad%TYPE,
        prm_fecha_hora_entrega         ordenes.fecha_hora_entrega%TYPE,
        prm_total                      ordenes.total%TYPE,
        prm_descuento                  ordenes.descuento%TYPE,
        prm_impuesto                   ordenes.impuesto%TYPE,
        prm_total_general              ordenes.total_general%TYPE,
        prm_id_direccion_facturacion   ordenes.id_direccion_facturacion%TYPE,
        prm_id_direccion_entrega       ordenes.id_direccion_entrega%TYPE
    ) IS
    BEGIN
        UPDATE ordenes
        SET
            id_oficina = prm_id_oficina,
            id_cliente = prm_id_cliente,
            id_tipo_identificacion = prm_id_tipo_identificacion,
            fecha_hora_orden = prm_fecha_hora_orden,
            fecha_hora_envio = prm_fecha_hora_envio,
            cantidad = prm_cantidad,
            fecha_hora_entrega = prm_fecha_hora_entrega,
            total = prm_total,
            descuento = prm_descuento,
            impuesto = prm_impuesto,
            total_general = prm_total_general,
            id_direccion_facturacion = prm_id_direccion_facturacion,
            id_direccion_entrega = prm_id_direccion_entrega
        WHERE
            id_orden = prm_id_orden;

    END;

--3.Agregar detalle de orden

PROCEDURE pr_agregar_detalle_orden (
    prm_id_orden       detalle_orden.id_orden%TYPE,
    prm_linea_orden    detalle_orden.linea_orden%TYPE,
    prm_cod_producto   detalle_orden.cod_producto%TYPE,
    prm_cantidad       detalle_orden.cantidad%TYPE,
    prm_monto          detalle_orden.monto%TYPE
) IS
    v_impuesto_venta ordenes.impuesto%TYPE;
    v_total       ordenes.total%TYPE;
    v_impuesto    ordenes.impuesto%TYPE;
    CURSOR cr_inventario_series IS
    SELECT
        numero_serie
    FROM
        inventario
    WHERE
        cod_producto = prm_cod_producto;

    v_num_serie
    inventario.numero_serie%TYPE;

BEGIN
    --Verificar disponibilidad de inventario
    OPEN cr_inventario_series;
    IF cr_inventario_series%rowcount < prm_cantidad THEN
        raise_application_error(-21, 'No hay sufucientes productos para completar la orden');
    END IF;
    FOR i IN 1..prm_cantidad LOOP
        FETCH cr_inventario_series INTO v_num_serie;
        INSERT INTO reservas_inventario (
            id_orden,
            linea_orden,
            numero_item,
            numero_serie
        ) VALUES (
            prm_id_orden,
            prm_linea_orden,
            i,
            v_num_serie
        );

    END LOOP;    
    
    --Insercion del detalle

    v_impuesto_venta := 0.13;
    v_total := prm_cantidad * prm_monto;
    v_impuesto := v_total * v_impuesto_venta;
    INSERT INTO detalle_orden (
        id_orden,
        linea_orden,
        cod_producto,
        cantidad,
        monto
    ) VALUES (
        prm_id_orden,
        prm_linea_orden,
        prm_cod_producto,
        prm_cantidad,
        prm_monto
    );
        --Actualización de la orden

    UPDATE ordenes
    SET
        total = total + v_total,
        impuesto = impuesto + v_impuesto,
        total_general = total_general + v_total + v_impuesto;

END;

--4.Modificar detalle orden

    PROCEDURE pr_modificar_detalle_orden (
        prm_id_orden       detalle_orden.id_orden%TYPE,
        prm_linea_orden    detalle_orden.linea_orden%TYPE,
        prm_cod_producto   detalle_orden.cod_producto%TYPE,
        prm_cantidad       detalle_orden.cantidad%TYPE,
        prm_monto          detalle_orden.monto%TYPE
    ) IS

        v_cantidad         detalle_orden.cantidad%TYPE;
        v_cod_producto     detalle_orden.cod_producto%TYPE;
        v_monto            detalle_orden.monto%TYPE;
        v_impuesto_venta   ordenes.impuesto%TYPE;
        v_total            ordenes.total%TYPE;
        v_impuesto         ordenes.impuesto%TYPE;
        CURSOR cr_inventario_series IS
        SELECT
            numero_serie
        FROM
            inventario
        WHERE
            cod_producto = prm_cod_producto;

        v_num_serie        inventario.numero_serie%TYPE;
    BEGIN
        OPEN cr_inventario_series;
    
    --Nuevos Montos
        v_impuesto_venta := 0.13;
        v_total := prm_cantidad * prm_monto;
        v_impuesto := v_total * v_impuesto_venta;    
    
    --Verificar que el detalle de orden exista
        SELECT
            cod_producto,
            cantidad,
            v_monto
        INTO
            v_cod_producto,
            v_cantidad,
            v_monto
        FROM
            detalle_orden
        WHERE
            id_orden = prm_id_orden
            AND linea_orden = prm_linea_orden;

    --Si son el mismo producto

        IF ( v_cod_producto = prm_cod_producto ) THEN
        --Si la cantidad nueva es menor que la vieja
            IF ( v_cantidad > prm_cantidad ) THEN
            --Eliminar N de reservas
                DELETE FROM reservas_inventario
                WHERE
                    id_orden = prm_id_orden
                    AND linea_orden = prm_linea_orden
                        AND numero_item > prm_cantidad;

            ELSE
            --La cantidad nueva es mayor a la actual
            --Verificar disponibilidad de inventario (deben haber al menos la cantidad nueva menos la vieja disponible
                IF cr_inventario_series%rowcount < ( prm_cantidad - v_cantidad ) THEN
                    FOR i IN 1..( prm_cantidad - v_cantidad ) LOOP
                        FETCH cr_inventario_series INTO v_num_serie;
                        INSERT INTO reservas_inventario (
                            id_orden,
                            linea_orden,
                            numero_item,
                            numero_serie
                        ) VALUES (
                            prm_id_orden,
                            prm_linea_orden,
                            v_cantidad + i,
                            v_num_serie
                        );

                    END LOOP;

                END IF;
            END IF;
        --Actualizacion de detalle de orden

            UPDATE detalle_orden
            SET
                cantidad = prm_cantidad,
                monto = prm_monto,
                cod_producto = prm_cod_producto
            WHERE
                id_orden = prm_id_orden
                AND linea_orden = prm_linea_orden;
        --Actualización de la orden

            UPDATE ordenes
            SET
                total = ( total + v_total ) - ( v_cantidad * v_monto ),
                impuesto = ( impuesto + v_impuesto ) - ( v_cantidad * v_monto * v_impuesto_venta ),
                total_general = total_general + v_total + v_impuesto - ( v_cantidad * v_monto ) - ( v_cantidad * v_monto * v_impuesto_venta
                );

        ELSE --Si no es el mismo codifo de producto
        --Eliminar los registros viejos
            DELETE FROM detalle_orden
            WHERE
                id_orden = prm_id_orden
                AND linea_orden = prm_linea_orden;

            DELETE FROM reservas_inventario
            WHERE
                id_orden = prm_id_orden
                AND linea_orden = prm_linea_orden;

            UPDATE ordenes
            SET
                total = total - ( v_cantidad * v_monto ),
                impuesto = impuesto - ( v_cantidad * v_monto * v_impuesto_venta ),
                total_general = total_general - ( v_cantidad * v_monto ) - ( v_cantidad * v_monto * v_impuesto_venta );    
        --agregar uno nuevo

            pr_agregar_detalle_orden(prm_id_orden, prm_linea_orden, prm_cod_producto, prm_cantidad, prm_monto);
        END IF;

    END;
    
--5. Procedure para eliminar una orden

    PROCEDURE pr_eliminar_orden (
        prm_numero_orden ordenes.id_orden%TYPE
    ) IS
    BEGIN
        DELETE FROM ordenes
        WHERE
            id_orden = prm_numero_orden;

    END;

--6. Eliminar una linea de detalle de orden

    PROCEDURE pr_eliminar_detalle_orden (
        prm_id_orden   detalle_orden.id_orden%TYPE,
        prm_linea      detalle_orden.linea_orden%TYPE
    ) IS
    BEGIN
        DELETE FROM detalle_orden
        WHERE
            id_orden = prm_id_orden
            AND linea_orden = prm_linea;

        DELETE FROM reservas_inventario
        WHERE
            id_orden = prm_id_orden
            AND linea_orden = prm_linea;

        end;
    END paq_administracion_ordenes;