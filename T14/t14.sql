/************************************************************************************************************************************************************************************
Parte 1: Ingresar datos XML en un XMLType
************************************************************************************************************************************************************************************/
CREATE TABLE tmp_test_xml (
    nomarchivo   VARCHAR2(50),
    datosxml     XMLTYPE
);

INSERT INTO tmp_test_xml VALUES (
    'file.xml',
    xmltype('<?xml version="1.0" encoding="UTF-8"?>  <list_book>  <book>1</book>  <book>2</book>  <book>3</book>  </list_book>')
);

SELECT
    *
FROM
    tmp_test_xml;

CREATE DIRECTORY folderxml AS 'C:\SQLAvanzadoXML\';

CREATE TABLE compras OF XMLTYPE;

INSERT INTO compras VALUES ( xmltype(bfilename('FOLDERXML', 'purchaseOrder.xml'), nls_charset_id('AL32UTF8')) );

SELECT
    XMLSERIALIZE(DOCUMENT object_value AS CLOB) AS datosxml
FROM
    compras;

/************************************************************************************************************************************************************************************
Parte 2: Consulta de datos
************************************************************************************************************************************************************************************/

--Consultar datos XML usando los métodos de XMLType*************************************************************************************************************

CREATE TABLE tablaxml OF XMLTYPE;

CREATE TABLE table_with_xml_column (
    filename       VARCHAR2(64),
    xml_document   XMLTYPE
);

INSERT INTO tablaxml VALUES ( xmltype(bfilename('FOLDERXML', 'purchaseOrder.xml'), nls_charset_id('AL32UTF8')) );

INSERT INTO table_with_xml_column (
    filename,
    xml_document
) VALUES (
    'purchaseOrder.xml',
    xmltype(bfilename('FOLDERXML', 'purchaseOrder.xml'), nls_charset_id('AL32UTF8'))
);

SELECT
    x.object_value.getclobval()
FROM
    tablaxml x;

SELECT
    x.xml_document.getclobval()
FROM
    table_with_xml_column x;

--Consultar datos XML usando funciones SQL*************************************************************************************************************

SELECT
    x.object_value.getclobval()
FROM
    tablaxml x
WHERE
    XMLEXISTS ( '/PurchaseOrder[SpecialInstructions="Expedite"]' PASSING x.object_value );

SELECT
    extract(object_value, '/PurchaseOrder/Reference').getclobval() "REFERENCE"
FROM
    tablaxml
WHERE
    existsnode(object_value, '/PurchaseOrder[SpecialInstructions="Expedite"]') = 1;

SELECT
    extractvalue(object_value, '/PurchaseOrder/Reference') "REFERENCE"
FROM
    tablaxml
WHERE
    XMLEXISTS ( '/PurchaseOrder[SpecialInstructions="Expedite"]' PASSING object_value );

--Consulta avanzada*************************************************************************************************************

SELECT
    extractvalue(object_value, '/PurchaseOrder/Reference') reference,
    extractvalue(object_value, '/PurchaseOrder/*//User') userid,
    CASE
        WHEN existsnode(object_value, '/PurchaseOrder/Reject/Date') = 1 THEN
            'Rejected'
        ELSE
            'Accepted'
    END "STATUS",
    CASE
        WHEN extractvalue(object_value, '/PurchaseOrder/*//User') = 'NGREENBE' THEN
            'MAXIMUM'
        ELSE
            'NORMAL'
    END "PRIORITY",
    TO_DATE(substr(extractvalue(object_value, '/PurchaseOrder/Reference'), instr(extractvalue(object_value, '/PurchaseOrder/Reference'
    ), '-') + 1, 8), 'YYYYMMDD') AS orderdate
FROM
    tablaxml
WHERE
    XMLEXISTS ( '/PurchaseOrder[SpecialInstructions="Expedite"]' PASSING object_value )
ORDER BY
    substr(extractvalue(object_value, '/PurchaseOrder/Reference'), instr(extractvalue(object_value, '/PurchaseOrder/Reference'), '-'
    ) + 1, 8);


--Función XMLCAST con XML query*************************************************************************************************************

SELECT
    XMLCAST(XMLQUERY('/PurchaseOrder/Reference' PASSING object_value RETURNING CONTENT) AS VARCHAR2(100)) "REFERENCE"
FROM
    tablaxml
WHERE
    XMLEXISTS ( '/PurchaseOrder[SpecialInstructions="Expedite"]' PASSING object_value );
    
/************************************************************************************************************************************************************************************
Parte 3: Uso de Xquery
************************************************************************************************************************************************************************************/

SELECT
    ( XMLQUERY('for $i in /PurchaseOrder
where $i/CostCenter eq "S30"
and $i/User eq "AWALSH"
return <S30 ordnum="{$i/Reference}"/>'
    PASSING object_value RETURNING CONTENT) ).getclobval ( )
FROM
    tablaxml;

SELECT
    xtab.column_value
FROM
    tablaxml,
    XMLTABLE ( 'for $i in /PurchaseOrder
where $i/CostCenter eq "A10"
and $i/User eq "SMCCAIN"
return <A10 ordnum="{$i/Reference}"/>'
    PASSING object_value ) xtab;

SELECT
    lines.lineitem,
    lines.description,
    lines.partid,
    lines.unitprice,
    lines.quantity
FROM
    purchaseorder,
    XMLTABLE ( '
for $i in /PurchaseOrder/LineItems/LineItem
 where $i/@ItemNumber >= 8
 and $i/Part/@UnitPrice > 50
 and $i/Part/@Quantity > 2
 return $i'
    PASSING object_value COLUMNS lineitem NUMBER PATH '@ItemNumber', description VARCHAR2(30) PATH 'Description', partid NUMBER PATH
    'Part/@Id', unitprice NUMBER PATH 'Part/@UnitPrice', quantity NUMBER PATH 'Part/@Quantity' ) lines;
    
    
/************************************************************************************************************************************************************************************
Parte 3: Modificacion de datos
************************************************************************************************************************************************************************************/
--UpdateXML*************************************************************************************************************
--Para poder actualizar los datos se asigna el OBJECT_VALUE con el valor que devuelve la función updateXML.

SELECT
    extract(object_value, '/PurchaseOrder/Actions/Action[1]').getclobval() action
FROM
    tablaxml
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="SBELL-2002100912333601PDT"]') = 1;

UPDATE tablaxml
SET
    object_value = updatexml(object_value, '/PurchaseOrder/Actions/Action[1]/User/text()', 'SKING')
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="SBELL-2002100912333601PDT"]') = 1;

SELECT
    extract(object_value, '/PurchaseOrder/Actions/Action[1]') action
FROM
    tablaxml
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="SBELL-2002100912333601PDT"]') = 1;



--Pueden modificarse múltiples elementos en una sola instrucción UPDATE:

UPDATE tablaxml
SET
    object_value = updatexml(object_value, '/PurchaseOrder/Requestor/text()', 'Stephen G. King', '/PurchaseOrder/LineItems/LineItem[1]/Part/@Id'
    , '786936150421', '/PurchaseOrder/LineItems/LineItem[1]/Description/text()', 'The Rock', '/PurchaseOrder/LineItems/LineItem[3]'
    , xmltype('<LineItem ItemNumber="99">
<Description>Dead Ringers</Description>
<Part Id="715515009249" UnitPrice="39.95" Quantity="2"/>
</LineItem>'
    ))
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="SBELL-2002100912333601PDT"]') = 1;
--insertXMLChild*************************************************************************************************************

SELECT
    extract(object_value, '/PurchaseOrder/LineItems/LineItem[@ItemNumber="1"]').getclobval()
FROM
    tablaxml
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="AMCEWEN-20021009123335370PDT"]') = 1;

UPDATE tablaxml
SET
    object_value = insertchildxml(object_value, '/PurchaseOrder/LineItems', 'LineItem', xmltype('<LineItem ItemNumber="1">
<Description>The Harder They Come</Description>
<Part Id="953562951413" 
UnitPrice="22.95" 
Quantity="1"/>
</LineItem>'
    ))
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="AMCEWEN-20021009123335370PDT"]') = 1;

SELECT
    extract(object_value, '/PurchaseOrder/LineItems/LineItem[@ItemNumber="1"]').getclobval()
FROM
    tablaxml
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="AMCEWEN-20021009123335370PDT"]') = 1;
--DELETECHILD*************************************************************************************************************

SELECT
    extract(object_value, '/PurchaseOrder/LineItems/LineItem[@ItemNumber="2"]')
FROM
    tablaxml
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="AWALSH-20021009123336101PDT"]') = 1;

UPDATE tablaxml
SET
    object_value = deletexml(object_value, '/PurchaseOrder/LineItems/LineItem[@ItemNumber="2"]')
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="AWALSH-20021009123336101PDT"]') = 1;

SELECT
    extract(object_value, '/PurchaseOrder/LineItems/LineItem[@ItemNumber="2"]')
FROM
    tablaxml
WHERE
    existsnode(object_value, '/PurchaseOrder[Reference="AWALSH-20021009123336101PDT"]') = 1;