--Creacion de usuario comun en Container Database
--   alter session set container=CDB$ROOT;
--   CREATE USER c##edius IDENTIFIED BY oracle 
--   drop user C##edius;

--Ver los PDB disponibles desde mi conexion actual.
--   select PDB from v$services;

--Abre la base de datos Pluggable creada durante la instalacion de Oracle
alter pluggable database ORCLPDB open read write; 
--Define que la base de datos Pluggable debe iniciar siempre en el estado actual (OPEN READ WRITE) tras un reinicio de la base de datos contenedor.
ALTER PLUGGABLE DATABASE ORCLPDB SAVE STATE;
--Nos pasamos a la base contenedor que abrimos.
alter session set container=ORCLPDB;
--Ver los PDB disponibles desde mi conexion actual.
--select PDB from v$services;
--Ver el nombre de mi conexion actual
--show con_name
--creamos nuestro usuario
CREATE USER ALUMNO IDENTIFIED BY oracle;
--Damos al usuario permiso de crear objetos de diferentes tipos
GRANT RESOURCE TO ALUMNO;
--Damos al usuario permiso de loguearse en oracle.
GRANT CREATE SESSION TO ALUMNO;

ALTER USER ALUMNO quota unlimited on USERS;

GRANT CREATE VIEW TO ALUMNO;

GRANT CREATE ANY directory TO ALUMNO;
 
 
--poder acceder a Oracle EM Express apuntando a una base de datos ORCLPDB o CDB$ROOT requiere ejecutar esto como sys
exec dbms_xdb_config.SetGlobalPortEnabled(TRUE)