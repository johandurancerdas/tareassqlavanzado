--Vista actualizable para ordenes
CREATE OR REPLACE VIEW vwClienteVIPContacto as
select * 
from contactos ct
where user = 'ATENCION_VIP'
and exists (select 1 from clientes cl where ct.id_cliente = cl.id_cliente and ct.id_tipo_identificacion = cl.id_tipo_identificacion and cl.segmento_cliente = 'VIP')
WITH CHECK OPTION;
/

select * from vwClienteVIPCOntacto where id_cliente = 767414985 and valor_contacto = '89669038';
insert into vwClienteVIPContacto (id_cliente, id_tipo_identificacion, id_tipo_contacto, valor_contacto, num_extension) values ( 767414985, 1, 'TP' , '89669038', null);
select * from vwClienteVIPCOntacto where id_cliente = 767414985 and valor_contacto = '89669038';

--Vista actualizable para información personal
CREATE OR REPLACE VIEW vwClienteVIPInfoPersonal as
select * from clientes ct
where user = 'ATENCION_VIP'
and exists (select 1 from clientes cl where ct.id_cliente = cl.id_cliente and ct.id_tipo_identificacion = cl.id_tipo_identificacion and cl.segmento_cliente = 'VIP')
WITH CHECK OPTION;
/

select * from vwClienteVIPInfoPersonal where id_cliente = 887340781 and id_tipo_identificacion = 2;
update vwClienteVIPInfoPersonal set fecha_ingreso = sysdate where id_cliente = 887340781 and id_tipo_identificacion = 2;
select * from vwClienteVIPCOntacto where id_cliente = 472019719 and valor_contacto = '89669038';

--Vista actualizable para ordenes.
CREATE OR REPLACE VIEW vwClienteVIPOrdenes as
select * from ordenes od
where user = 'FACTORAAS_TEST'
and exists (select 1 from clientes cl where od.id_cliente = cl.id_cliente and od.id_tipo_identificacion = cl.id_tipo_identificacion and cl.segmento_cliente = 'VIP')
WITH CHECK OPTION;
/


select * from vwClienteVIPOrdenes where id_orden = 26984;
update vwClienteVIPOrdenes set fecha_hora_entrega = sysdate where id_orden = 26984;
select * from vwClienteVIPOrdenes where id_orden = 26984;
