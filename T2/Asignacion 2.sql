--1. Altere la base de datos para que cada oficina tenga un campo con su oficina padre.
alter table estructura_organizacional add oficina_padre NUMBER(38) constraint oficina_padre_fk references estructura_organizacional(id_oficina);

--2. Venta total por productos ordenados por cada oficina, por mes. 
--Incluir detalles de la oficina, y cantidad de �temes vendidos de cada producto as� como el monto de la venta.
--select eo.id_oficina, p.cod_producto, o.id_orden, p.precio, do.cantidad, do.monto, o.total
select eo.id_oficina, do.cod_producto, count(*) as cantidad_vendidos, sum(do.monto)
from productos p
join detalle_orden do
on p.cod_producto = do.cod_producto
join ordenes o 
on do.id_orden = o.id_orden
join estructura_organizacional eo
on o.id_oficina = eo.id_oficina
group by eo.id_oficina, do.cod_producto;
--3. Existencias de inventario por producto, descontando los �tems de inventario que ya se
--reservaron para atender ordenes.

--4. Top de los mejores clientes, seg�n su monto de compra, que incluya cantidad de
--ordenes hechas y cantidad de productos comprados.

--5. Costo de los materiales por proveedor, que tuvieron que adquirirse para confeccionar
--los productos que fueron ordenados por los clientes el mes actual. 