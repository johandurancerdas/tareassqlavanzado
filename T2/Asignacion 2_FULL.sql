/****************************************************************************************
--1. Altere la base de datos para que cada oficina tenga un campo con su oficina padre.
****************************************************************************************/
ALTER TABLE estructura_organizacional ADD oficina_padre NUMBER(38)
    CONSTRAINT oficina_padre_fk
        REFERENCES estructura_organizacional ( id_oficina );
/****************************************************************************************

--2. Venta total por productos ordenados por cada oficina, por mes. 
--Incluir detalles de la oficina, y cantidad de �temes vendidos de cada producto as� como el monto de la venta.
****************************************************************************************/

--(id_oficina, nombre_oficina, id_tipo_oficina, desc_tipo_oficina, annio, mes, cod_producto, desc_producto, cantidadxproducto, montototalxproducto ) 

CREATE OR REPLACE VIEW vwventaproductos_oficinames AS
    SELECT
        id_oficina,
        nombre_oficina,
        id_tipo_oficina,
        desc_tipo_oficina, --Datos de oficina
        annio,
        mes,
        cod_producto,
        desc_producto,
        SUM(cantidad) AS cantidadxproducto,
        SUM(montolinea) AS montototalxproductos
    FROM
        (
            SELECT
                eo.id_oficina,
                eo.nombre          AS nombre_oficina,
                teo.id_tipo_oficina,
                teo.descripcion    AS desc_tipo_oficina, --Datos de oficina
                TO_CHAR(ord.fecha_hora_entrega, 'rrrr') AS annio,
                TO_CHAR(ord.fecha_hora_entrega, 'MM') AS mes, --Fecha
                dord.cod_producto,
                prod.descripcion   AS desc_producto,
                prod.precio,
                dord.cantidad,
                dord.monto         AS montounitario,
                ( dord.cantidad * dord.monto ) AS montolinea
            FROM
                detalle_orden               dord
                INNER JOIN productos                   prod ON dord.cod_producto = prod.cod_producto
                INNER JOIN ordenes                     ord ON ord.id_orden = dord.id_orden
                INNER JOIN estructura_organizacional   eo ON ord.id_oficina = eo.id_oficina
                INNER JOIN tipos_oficina               teo ON eo.id_tipo_oficina = teo.id_tipo_oficina
        )
    GROUP BY
        id_oficina,
        nombre_oficina,
        id_tipo_oficina,
        desc_tipo_oficina, --Datos de oficina
        annio,
        mes,
        cod_producto,
        desc_producto
    ORDER BY
        id_oficina DESC,
        annio DESC,
        mes DESC,
        cod_producto;
/****************************************************************************************
--3. Existencias de inventario por producto, descontando los �tems de inventario que ya se
--reservaron para atender ordenes.
****************************************************************************************/

CREATE OR REPLACE VIEW vwinventarioproducto AS
    SELECT
        inv.cod_producto,
        COUNT(1) AS cantidad
    FROM
        inventario inv
    WHERE
        inv.numero_serie NOT IN (
            SELECT
                rinv.numero_serie
            FROM
                reservas_inventario rinv
        )
    GROUP BY
        cod_producto
    ORDER BY
        cod_producto;
/
/****************************************************************************************
--4. Top de los mejores clientes, seg�n su monto de compra, que incluya cantidad de ordenes hechas y cantidad de productos comprados.
****************************************************************************************/
CREATE OR REPLACE VIEW vwtopmejoresclientes AS
    SELECT
        base.id_cliente, cf.nombre, cf.primer_apellido, cf.segundo_apellido, cj.razon_social,cantidad_productos_comprados,  cantidad_ordenes_hechas, monto_total_comprado
    FROM
        (
            SELECT
                c.id_cliente,
                c.id_tipo_identificacion,
                SUM(do.cantidad) cantidad_productos_comprados,
                COUNT(o.id_orden) cantidad_ordenes_hechas,
                SUM(o.total) monto_total_comprado
            FROM
                clientes        c
                JOIN ordenes         o ON c.id_cliente = o.id_cliente and c.id_tipo_identificacion = o.id_tipo_identificacion
                JOIN detalle_orden   do ON o.id_orden = do.id_orden
            GROUP BY
                c.id_cliente,
                c.id_tipo_identificacion
        ) base
        LEFT JOIN clientes_fisicos     cf ON base.id_cliente = cf.id_cliente
                                    AND base.id_tipo_identificacion = cf.id_tipo_identificacion
        LEFT JOIN clientes_juridicos   cj ON base.id_cliente = cj.id_cliente
                                      AND base.id_tipo_identificacion = cj.id_tipo_identificacion
    ORDER BY
        monto_total_comprado DESC;
/
/****************************************************************************************
--5. Costo de los materiales por proveedor, que tuvieron que adquirirse para confeccionar
--los productos que fueron ordenados por los clientes el mes actual. 
****************************************************************************************/

CREATE OR REPLACE VIEW vwcostomaterialesporproducto AS
    SELECT
        ixp.identificacion_proveedor,
        ixp.id_tipo_identificacion,
        i.id_insumo,
        COUNT(i.id_insumo) cantidad_de_insumos,
        SUM(ixp.precio) precio_insumo_mes_actual
    FROM
        insumos_x_proveedor   ixp
        JOIN insumos               i ON ixp.id_insumo = i.id_insumo
        JOIN insumos_x_producto    ixp ON i.id_insumo = ixp.id_insumo
        JOIN productos             p ON ixp.cod_producto = p.cod_producto
        JOIN detalle_orden         do ON p.cod_producto = do.cod_producto
        JOIN ordenes               o ON do.id_orden = o.id_orden
    WHERE
        o.fecha_hora_orden BETWEEN trunc(last_day(add_months(SYSDATE, - 1)) + 1) AND trunc(last_day(SYSDATE))
    GROUP BY
        ixp.identificacion_proveedor,
        ixp.id_tipo_identificacion,
        i.id_insumo;
/