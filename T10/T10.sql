/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 1
Una vista de objetos sobre la tabla de FactoraaS que tiene la informaci�n de las sucursales, tiendas y oficinas.
****************************************************************************************************************************************************************************************************************************************************************************************/
CREATE OR REPLACE TYPE estructura_organizacional_tipo AS OBJECT (
    id_oficina            NUMBER(38),
    fecha_actualizacion   DATE,
    id_tipo_oficina       NUMBER(38),
    nombre                NVARCHAR2(50),
    codigo_postal         NUMBER(38),
    cod_pais              NCHAR(2),
    latitud               NUMBER(9, 6),
    longitud              NUMBER(9, 6),
    detalles              NVARCHAR2(250),
    provincia_estado      NVARCHAR2(150),
    canton_ciudad         NVARCHAR2(150),
    nombre_pais           NVARCHAR2(150),
    fecha_eliminacion     DATE
);
/

CREATE OR REPLACE VIEW estructura_organizacional_view
    OF estructura_organizacional_tipo WITH OBJECT IDENTIFIER ( id_oficina )
AS
    SELECT
        id_oficina,
        fecha_actualizacion,
        id_tipo_oficina,
        nombre,
        codigo_postal,
        cod_pais,
        latitud,
        longitud,
        detalles,
        provincia_estado,
        canton_ciudad,
        nombre_pais,
        fecha_eliminacion
    FROM
        estructura_organizacional;
/
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 2
Una vista objeto relacional (de tablas anidadas) para las �rdenes y los detalles de FactoraaS
****************************************************************************************************************************************************************************************************************************************************************************************/
--Se utiliza ordenes tipo de la asignaci�n 9, no lo pego ac� para no cargar mucho el .sql y que se pueda entender

CREATE OR REPLACE VIEW ordenes_and_detalles_view
    OF ordenes_tipo WITH OBJECT IDENTIFIER ( id_orden )
AS
    SELECT
        o.id_orden,
        o.id_oficina,
        o.id_cliente,
        o.id_tipo_identificacion,
        o.fecha_hora_orden,
        o.fecha_hora_envio,
        o.cantidad,
        o.fecha_hora_entrega,
        o.total,
        o.descuento,
        o.impuesto,
        o.total_general,
        o.id_direccion_facturacion,
        o.id_direccion_entrega,
        CAST(MULTISET(
            SELECT
                d.id_orden, d.linea_orden, d.cod_producto, d.cantidad, d.monto
            FROM
                detalle_orden d
            WHERE
                d.id_orden = o.id_orden
        ) AS detalle_orden_tabla) AS detalles
    FROM
        ordenes o;
/

/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 3
Utilice MAKE_REF en alguno de los ejercicios previos o en una vista nueva, para sintetizar referencias a objetos a partir de una llave for�nea.
****************************************************************************************************************************************************************************************************************************************************************************************/
--Modificaci�n de ordenes tipo de la tarea 9 para incluir referencia a la oficina
CREATE OR REPLACE TYPE ordenes_tipo AS OBJECT (
    id_orden                   NUMBER(38),
    id_oficina                 NUMBER(38),
    id_cliente                 NVARCHAR2(50),
    id_tipo_identificacion     NUMBER(38),
    fecha_hora_orden           DATE,
    fecha_hora_envio           DATE,
    cantidad                   NUMBER(38),
    fecha_hora_entrega         DATE,
    total                      NUMBER,
    descuento                  NUMBER(5, 2),
    impuesto                   NUMBER,
    total_general              NUMBER,
    id_direccion_facturacion   NUMBER(38),
    id_direccion_entrega       NUMBER(38),
    direccion_entrega_ref      REF estructura_organizacional_tipo,
    MAP MEMBER FUNCTION comparar RETURN NUMBER, PRAGMA restrict_references ( comparar, wnds, trust )
);
/

CREATE OR REPLACE TYPE BODY ordenes_tipo AS
    MAP MEMBER FUNCTION comparar RETURN NUMBER IS
    BEGIN
        RETURN total;
    END;

END;
/

CREATE OR REPLACE VIEW ordenes_and_detalles_view
    OF ordenes_tipo WITH OBJECT IDENTIFIER ( id_orden )
AS
    SELECT
        o.id_orden,
        o.id_oficina,
        o.id_cliente,
        o.id_tipo_identificacion,
        o.fecha_hora_orden,
        o.fecha_hora_envio,
        o.cantidad,
        o.fecha_hora_entrega,
        o.total,
        o.descuento,
        o.impuesto,
        o.total_general,
        o.id_direccion_facturacion,
        o.id_direccion_entrega,
        make_ref(estructura_organizacional_view, o.id_oficina)
    FROM
        ordenes o;
/

SELECT
    *
FROM
    ordenes_and_detalles_view;