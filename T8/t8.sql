/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 1,2,3 esfera, prisma y comparaci�n de objetos basados en super clase.
    � Bas�ndose en lo visto en la semana 8 y 9. Cree un nuevo tipo Esfera_t con los atributos adecuados y cree los m�todos para calcular el �rea y volumen de la esfera.

    � Agregue a los tipos Prisma_t y a Esfera_t un m�todo ORDER o MAP (a ambos).

    � Ya que las esferas y prismas son figuras con volumen: �podr�an compararse entre s�?. Con base en lo visto hasta el momento haga los cambios necesarios para poder comparar esferas y prismas a trav�s del valor de su volumen en una sola clase base. Piense en usar herencia, m�todos de comparaci�n y sobrecarga de m�todos para lograr su cometido.
****************************************************************************************************************************************************************************************************************************************************************************************/
CREATE OR REPLACE TYPE figura_t AS OBJECT (
    v_volumen   NUMBER,
    v_area      NUMBER,
    MAP MEMBER FUNCTION comparar RETURN NUMBER, PRAGMA restrict_references ( comparar, wnds, trust )
);
/

CREATE OR REPLACE TYPE BODY figura_t AS
    MAP MEMBER FUNCTION comparar RETURN NUMBER IS
    BEGIN
        RETURN v_volumen;
    END;

END;
/

ALTER TYPE figura_t NOT FINAL
    CASCADE;
/

CREATE OR REPLACE TYPE prisma_t UNDER figura_t (
    v_alto          INTEGER,
    v_ancho         INTEGER,
    v_profundidad   INTEGER,
    CONSTRUCTOR FUNCTION prisma_t (
           alto          NUMBER,
           ancho         NUMBER,
           profundidad   NUMBER
       ) RETURN SELF AS RESULT,
    MEMBER FUNCTION obtener_area RETURN NUMBER,
    MEMBER FUNCTION obtener_volumen RETURN NUMBER
);
/

CREATE OR REPLACE TYPE BODY prisma_t AS
    CONSTRUCTOR FUNCTION prisma_t (
        alto          NUMBER,
        ancho         NUMBER,
        profundidad   NUMBER
    ) RETURN SELF AS RESULT IS
    BEGIN
        self.v_alto := alto;
        self.v_ancho := ancho;
        self.v_profundidad := profundidad;
        self.v_volumen := alto * ancho * profundidad;
        self.v_area := 2 * ( alto * ancho + alto * profundidad + ancho * profundidad );

        return;
    END;

    MEMBER FUNCTION obtener_volumen RETURN NUMBER IS
    BEGIN
        RETURN v_volumen;
    END;

    MEMBER FUNCTION obtener_area RETURN NUMBER IS
    BEGIN
        RETURN v_area;
    END;

END;
/

/**********************************************************************************************
Pregunta 1 - Bas�ndose en lo visto en la semana 8 y 9. Cree un nuevo tipo Esfera_t con los atributos adecuados y cree los m�todos para calcular el �rea y volumen de la esfera.
**********************************************************************************************/

CREATE OR REPLACE TYPE esfera_t UNDER figura_t (
    v_radio NUMBER,
    CONSTRUCTOR FUNCTION esfera_t (
           radio NUMBER
       ) RETURN SELF AS RESULT,
    MEMBER FUNCTION obtener_volumen RETURN NUMBER,
    MEMBER FUNCTION obtener_area RETURN NUMBER
);
/

CREATE OR REPLACE TYPE BODY esfera_t AS
    CONSTRUCTOR FUNCTION esfera_t (
        radio NUMBER
    ) RETURN SELF AS RESULT IS
    BEGIN
        self.v_radio := radio;
        self.v_volumen := ( 4 / 3 ) * 3.141592654 * power(radio, 3);

        self.v_area := 4 * 3.141592654 * power(radio, 2);
        return;
    END;

    MEMBER FUNCTION obtener_volumen RETURN NUMBER IS
    BEGIN
        RETURN v_volumen;
    END;

    MEMBER FUNCTION obtener_area RETURN NUMBER IS
    BEGIN
        RETURN v_area;
    END;

END;
/

DECLARE
    esfera_v   esfera_t;
    prisma_v   prisma_t;
BEGIN
    prisma_v := prisma_t(120, 90, 30); -- Se usa el constructor por defecto.
    esfera_v := esfera_t(10);
    IF ( prisma_v.comparar() = esfera_v.comparar() ) THEN
        dbms_output.put_line('EQUAL');
    END IF;

END;
/



/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 4. Empleados
    � Agregue al tipo Empleado (que se vio en los materiales de la semana) que cuente con:
    los datos de una persona como atributos
    el tiempo que lleva laborando para la empresa
    agregue una calificaci�n de rendimiento del empleado 
    un m�todo que permita comparar entre dos empleados por su rendimiento.
    un m�todo para determinar la edad de la persona 
****************************************************************************************************************************************************************************************************************************************************************************************/

DROP TYPE empleado_t;
/

CREATE OR REPLACE TYPE empleado_t AS OBJECT (
    v_nombre             VARCHAR(20),
    v_apellido           VARCHAR(20),
    v_edad               INTEGER,
    v_fecha_nacimiento   DATE,
    v_anyos_laborando    NUMBER,
    v_rendimiento        NUMBER,
    MAP MEMBER FUNCTION comparar RETURN NUMBER, PRAGMA restrict_references ( comparar, wnds, trust ),
    MEMBER FUNCTION obtener_edad RETURN INTEGER
);
/

CREATE OR REPLACE TYPE BODY empleado_t AS
    MAP MEMBER FUNCTION comparar RETURN NUMBER IS
    BEGIN
        RETURN v_rendimiento;
    END;

    MEMBER FUNCTION obtener_edad RETURN INTEGER IS
    BEGIN
        RETURN v_edad;
    END;

END;
/