-- Crear el campo si no existe oficina padre
ALTER TABLE estructura_organizacional ADD oficina_padre NUMBER(38)
    CONSTRAINT oficina_padre_fk
        REFERENCES estructura_organizacional ( id_oficina );
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 1
Realice los cambios correspondientes (de ser necesario) para que la tabla de Oficinas de FactoraaS utilice un jerárquico de modelo de lista de adyacencia. 
****************************************************************************************************************************************************************************************************************************************************************************************/

--Primer Nivel

UPDATE estructura_organizacional
SET
    oficina_padre = NULL
WHERE
    id_oficina < 10;
--Segundo Nivel

UPDATE estructura_organizacional
SET
    oficina_padre = 1
WHERE
    id_oficina BETWEEN 10 AND 99;
--Cuarto Nivel

UPDATE estructura_organizacional
SET
    oficina_padre = substr(id_oficina, 1, 1)
                    || '00'
WHERE
    id_oficina > 100
    AND substr(id_oficina, 3, 3) = 0
    AND id_oficina NOT IN (
        100,
        200,
        300,
        400,
        500,
        600,
        700,
        800,
        900
    );
--Tercer Nivel

UPDATE estructura_organizacional
SET
    oficina_padre = substr(id_oficina, 1, 2)
WHERE
    id_oficina IN (
        100,
        200,
        300
    );

UPDATE estructura_organizacional
SET
    oficina_padre = 1
WHERE
    id_oficina IN (
        400,
        500,
        600,
        700
    );
--Quinto Nivel

UPDATE estructura_organizacional
SET
    oficina_padre = substr(id_oficina, 1, 2)
                    || 0
WHERE
    oficina_padre IS NULL
    AND id_oficina <> 1;

--Consulta de prueba

SELECT
    eo.nombre,
    eo.codigo_postal,
    eo.id_oficina,
    eo.id_tipo_oficina,
    tof.descripcion,
    id_oficina || eo.id_tipo_oficina oficina_and_tipo,
    eo.provincia_estado,
    eo.canton_ciudad,
    eo.oficina_padre
FROM
    estructura_organizacional   eo
    JOIN tipos_oficina               tof ON eo.id_tipo_oficina = tof.id_tipo_oficina
ORDER BY
    eo.id_oficina;
    
    
/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 2
Con base en la tabla de Oficinas de FactoraaS, cree una nueva estructura basada en el modelo de
enumeración de ruta y otra basada en el modelo de jerarquía de conjuntos anidados, y llene
correspondientemente las estructuras para que reflejen los mismos datos que la tabla de Oficinas,
pero en los modelos dados. 
****************************************************************************************************************************************************************************************************************************************************************************************/
--Crear campo para utilizar enumeración de rutas
-- Crear el campo si no existe oficina padre

ALTER TABLE estructura_organizacional ADD jerarquia_enum_ruta VARCHAR(500);

--Dado que tiene cuatro niveles con 4 joins se obtienen todos

UPDATE estructura_organizacional jer
SET
    jerarquia_enum_ruta = (
        SELECT
            e4.oficina_padre
            || '/'
            || e3.oficina_padre
            || '/'
            || e2.oficina_padre
            || '/'
            || e1.oficina_padre
            || '/'
            || e1.id_oficina
            || '/'
        FROM
            estructura_organizacional   e1
            LEFT JOIN estructura_organizacional   e2 ON e1.oficina_padre = e2.id_oficina
            LEFT JOIN estructura_organizacional   e3 ON e2.oficina_padre = e3.id_oficina
            LEFT JOIN estructura_organizacional   e4 ON e3.oficina_padre = e4.id_oficina
        WHERE
            e1.id_oficina = jer.id_oficina
    ); 

--Eliminar todos los / del inicio del path dado que algunos sobran.

UPDATE estructura_organizacional
SET
    jerarquia_enum_ruta = substr(jerarquia_enum_ruta, 2, 200)
WHERE
    jerarquia_enum_ruta LIKE '/%';

UPDATE estructura_organizacional
SET
    jerarquia_enum_ruta = substr(jerarquia_enum_ruta, 2, 200)
WHERE
    jerarquia_enum_ruta LIKE '/%';

UPDATE estructura_organizacional
SET
    jerarquia_enum_ruta = substr(jerarquia_enum_ruta, 2, 200)
WHERE
    jerarquia_enum_ruta LIKE '/%';

UPDATE estructura_organizacional
SET
    jerarquia_enum_ruta = substr(jerarquia_enum_ruta, 2, 200)
WHERE
    jerarquia_enum_ruta LIKE '/%';
    
    

/****************************************************************************************************************************************************************************************************************************************************************************************
Parte 2.2
Jerarquía de conjuntos
****************************************************************************************************************************************************************************************************************************************************************************************/
-- Crear el campo si no existe oficina padre
--Creación de la Pila para insertar los nodos

CREATE TABLE stack (
    stack_top   INTEGER NOT NULL,
    node        NUMBER NOT NULL,
    lft         INTEGER,
    rgt         INTEGER
);
--Crear una copia de la tabla estructura que ya contiene un model de lista de adyacencias y que vamos a utilizar para crear el basado en conjuntos

CREATE TABLE tree
    AS
        ( SELECT
            id_oficina      AS node,
            oficina_padre   AS parent
        FROM
            estructura_organizacional
        );

COMMIT;
/
-- Modificacion del codigo para ser utilizado en ORACLE 

DECLARE
    counter       INTEGER;
    max_counter   INTEGER;
    current_top   INTEGER;
    l_exist       NUMBER;
BEGIN
    counter := 2;
    SELECT
        2 * COUNT(*)
    INTO max_counter
    FROM
        tree;

    current_top := 1;
    DELETE FROM stack;

    INSERT INTO stack
        SELECT
            1,
            node,
            1,
            max_counter
        FROM
            tree
        WHERE
            parent IS NULL;

    DELETE FROM tree
    WHERE
        parent IS NULL;

    WHILE counter <= max_counter - 1 LOOP
        SELECT
            COUNT(1)
        INTO l_exist
        FROM
            stack   s1,
            tree    t1
        WHERE
            s1.node = t1.parent
            AND s1.stack_top = current_top;

        IF ( l_exist > 0 ) THEN
            INSERT INTO stack
                SELECT
                    ( current_top + 1 ),
                    MIN(t1.node),
                    counter,
                    CAST(NULL AS INTEGER)
                FROM
                    stack   s1,
                    tree    t1
                WHERE
                    s1.node = t1.parent
                    AND s1.stack_top = current_top;

            DELETE FROM tree
            WHERE
                node = (
                    SELECT
                        node
                    FROM
                        stack
                    WHERE
                        stack_top = current_top + 1
                );

            counter := counter + 1;
            current_top := current_top + 1;
        ELSE
            UPDATE stack
            SET
                rgt = counter,
                stack_top = - stack_top
            WHERE
                stack_top = current_top;

            counter := counter + 1;
            current_top := current_top - 1;
        END IF;

    END LOOP;

END;
/
-- Consulta para validar el resultado de la jerarquia de path y la de conjuntos

SELECT
    eo.id_oficina,
    eo.nombre,
    eo.jerarquia_enum_ruta,
    lft,
    rgt
FROM
    stack                       st
    JOIN estructura_organizacional   eo ON st.node = eo.id_oficina;